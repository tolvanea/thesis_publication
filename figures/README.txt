These figures can be reproduced by changing current directory to 'scripts', and running corresponding scripts.

For plotting the figures, you need to install Python3 packages "matplotlib", "numpy", and "uncertainties". Also, latex needs to be installed on the system.


--


There are few version of the figures with differing unit labels. They can be obtained from links below.

1. Original draft sent to publisher. Here all units in figures are like "<quantity> (<unit>)" such as "T (K)"
https://gitlab.com/tolvanea/thesis_publication/-/tree/f8c47d7a197e4c97d1f4e1ab3933e153de364504/figures

2. Version where all units in figures are like "<quantity> (unit of <unit>)" such as "T (unit of K)"
https://gitlab.com/tolvanea/thesis_publication/-/tree/f6b476f98285ea6b5a548c5c7377790cbd955424/figures

3. Otherwise same as the original 1., but figure "Ps2_varM.pdf" has unit on x-axis as "M (unit of m_p)"
https://gitlab.com/tolvanea/thesis_publication/-/tree/ed727ec111fb38c0674467d3d93421740217b9ae/figures

4. This current version: Same as 3. but ×10^-11 is moved from axis multiplier, to unit such as "T (10^-11 K)"
https://gitlab.com/tolvanea/thesis_publication/-/tree/master/figures
