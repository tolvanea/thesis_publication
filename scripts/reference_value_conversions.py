"""
This file calculates the reference values of susceptibility.
Because cgs-Gaussian unit system is widely used it need to be converted to SI.
Also some papers use ridiculous units, that requires quite some tinkering to
figure out what they are in human units.

"BO" means Born Oppenheimer
"AQ" means All Quantum, that is, nonadiabatic nuclei

"JT" means Juha Tiihonen
"AT" means Alpi Tolvanen
"""
import numpy as np
from tools.printing import err_format

class si:
    e=1.602176620898e-19;
    hbar = 6.6260755e-34/(2*np.pi);
    eps0=8.85419e-12;
    m_e=9.1093897e-31;      # mass of electron
    m_p=1.6726231e-27;      # mass of proton
    µ_0 = 4*np.pi * 1e-7
    a0 = 5.291772109e-11

    # Unitless:
    alpha = 1/137.035999084
    N_A = 6.02214076e23

class au:
    e = 1.0
    hbar = 1.0
    eps0 = 1.0/(4*np.pi)
    m_e = 1.0
    m_p = 1836.1526734
    c = 137.035999084
    µ_0 = 4*np.pi / c**2
    a0 = 1.0

    # Unitless:
    alpha = 1.0/c
    N_A = 6.02214076e23


# From a.u. "magnetizability" (i.e. no µ_0) to susceptibility
au_magnz_to_SI = si.µ_0* si.e**2 * si.a0**2 / si.m_e * si.N_A
# Actual from a.u. to SI of susceptibility
from_au_to_SI_real = si.µ_0/au.µ_0 * si.e**2 * si.a0**2 / si.m_e * si.N_A
from_cgs_to_si = 1e-6 * 4*np.pi

# Coefficient for Ps2 slope:
#print(3*3.166811e-6*au.µ_0*12 * from_au_to_SI_real)

# TITLE:    A note on the magnetic susceptibility of hydrogen and its isotopomers
# BIBTEX:   raynesNoteMagneticSusceptibility1974
# URL:      https://www.sciencedirect.com/science/article/abs/pii/0009261474802368
# Why raynes did you use something like that
raynes_units = 4.0051e-6 / 1.6855 * from_cgs_to_si

def m_r(m1, m2):
    # Reduced mass
    return 1 / (1/m1 + 1/m2)

def r2_mean(c, m):
    # This gives squared mean radius of nucleus and electron <r^2>.
    # Arguments:
    #   c: units (si/au class)
    #   m: reduced mass or m_e for BO

    # The 'a' below is <r>.  The equation "3 * a**2" can be derived on paper by
    # integration. (Integral B14 in Alpi's MSc thesis)
    # If m=m_e, then 'a' equal to bohr radius a0. See p. 88 in Atkins: "Molecular
    # Quantum Mechanics", 5th ed. \cite[p.~88]{atkinsMolecularQuantumMechanics}
    a = 4 * np.pi * c.eps0 * c.hbar**2 / (m * c.e**2)
    return 3 * a**2

def H_susceptibility(c, m1, m2, random_testing=False):
    """
    Calculates analytical value of susceptibility of H-like atom
    Arguments:
        c:     units (class 'si' or 'au')
        m1/m2: masses of electron and nuclei. The order does not matter.

    BO: m1=m_e, m2=inf
        - Equation derived in Alpi's MSc thesis' appendix B
    AQ: m1=m_e, m2=m_p
        - Equation can be derived from Rebane's equation 34.
    """
    reduced_mass = m_r(m1, m2)
    # In case of BO, a = a0 (Bohr's radius)
    a = 4 * np.pi * c.eps0 * c.hbar**2 / (reduced_mass * c.e**2)
    chi = -c.µ_0 * c.e**2/(2*reduced_mass) * a**2
    if c is si:
        chi *= c.N_A  # susceptibility per mole
    return chi

def rebane_E_to_susc(E_dia):
    # TITLE:    Nonadiabatic theory of diamagnetic susceptibility of molecules
    # BIBTEX:   rebaneNonadiabaticTheoryDiamagnetic2002
    # LINK:     http://link.springer.com/10.1134/1.1503752
    # Transform values from Rebane's paper's au energy units to si susceptibility
    # Keep in mind that:
    # - alpha in au is 1/c
    #   alpha = mu_0 * e^2 * c / (4*pi* hbar)
    # - X_SI = 4pi*X_gaussian
    chi_au = (-2 * au.alpha**2 * E_dia)
    chi_SI = (4 * np.pi * si.a0**3 * si.N_A) * chi_au
    return chi_SI

def rebane_eq_34(c, p, N, q, m, M, r2_1N, r2_12, r2_N1N):
    """
    Calculate magnetic susceptibilities for molecules, using equation 34 from
    Rebane's paper http://link.springer.com/10.1134/1.1503752.

    Use atomic units for inputs.

    Arguments
        c       units (class 'si' or 'au')
        p       number of electrons
        N       number of nucleus + electrons
        q       charge of electron
        m       mass of electron
        M       nucleus mass
        r2_1N   square mean distance <r^2> of nucleus-electron
        r2_12   square mean distance <r^2> of electron-electron
        r2_N1N  square mean distance <r^2> of nucleus-nucleus
        """
    alpha_units = 1/(c.alpha**2)
    E_dia = alpha_units \
        * (p*q**2/(24*(N-p)*c.c**2)) \
        * ((1/m)*(2*(N-p)*r2_1N - (N-p-1)*r2_N1N) \
        + (1/M)*(2*p*r2_1N - (p-1)*r2_12))
    return rebane_E_to_susc(E_dia)


# For H2 BO
def rot_avg(parallel, perpendicular, warn=True):
    # Knowing parallel and perpedicular components of susceptibility in
    # H2 (BO), calculate rotational average (3d) in bulk system. This
    # equation is simply the mean (X_x + X_y + X_z) / 3

    # This is exact rotational average, which would not hold for simple
    # classical components. The reason stems from <m^2> nature of
    # susceptibility, see
    #     https://doi.org/10.1021/ed081p877
    #     https://www.smoldyn.org/andrews/papers/Andrews_2004.pdf
    # or
    #     https://doi.org/10.1063/1.442747
    #
    # This equation is widely used, for example it is used in eq.1 of
    #     TITLE:    A note on the magnetic susceptibility of hydrogen and its isotopomers
    #     BIBTEX:   raynesNoteMagneticSusceptibility1974
    #     URL:      https://www.sciencedirect.com/science/article/abs/pii/0009261474802368
    if warn and parallel**2 > perpendicular**2:
        raise Exception("Did you accidentally swap the arguments?")
    return 1/3 * (parallel + 2* perpendicular)

def print_H():
    chi_H_AQ =  rebane_E_to_susc(0.250409)  # Direct number from rebanes paper

    # Derived with equation 34 of Rebanes paper. Using analytical <r^2>
    chi_H_AQ_v2 = H_susceptibility(si, si.m_e, si.m_p)

    print("χ_H AQ, ref direct rebane    ", "{:.6e}".format(chi_H_AQ))
    print("χ_H AQ, analyt. <r^2>+rebane ", "{:.6e}".format(chi_H_AQ_v2))
    print("--")

    # Derived in my thesis
    # Using a.u. for floating point accuracy (to get more than 5 decimals)
    chi_BO_analytical = H_susceptibility(au, au.m_e, float("inf"))*from_au_to_SI_real

    print("χ_H BO, analytical           ", "{:.5e}".format(chi_BO_analytical))


def print_He_BO():
    # <r^2> values from haftel, eq34 from rebane
    # TITLE:    Precise nonvariational calculations on the helium atom
    # BIBTEX:   haftelPreciseNonvariationalCalculations1988
    # URL:      10.1103/PhysRevA.38.5995
    haftel_rebane = rebane_eq_34(
        au, p=2, N=3, q=-au.e, m=au.m_e, M=np.inf,
        r2_1N=1.19348312 * au.a0**2,
        r2_12=2.516440118 * au.a0**2,
        r2_N1N=0.0
    )

    # TITLE:    Hartree-Fock Diamagnetic Susceptibilities
    # BIBTEX:   mendelsohnHartreeFockDiamagneticSusceptibilities1970b
    # URL:      https://link.aps.org/doi/10.1103/PhysRevA.2.1130
    mendelson = -1.8767e-6 * from_cgs_to_si

    # \cite{bruchDiamagnetismHelium2000}
    # TITLE:    Diamagnetism of helium
    # BIBTEX:   bruchDiamagnetismHelium2000
    # URL:      http://aip.scitation.org/doi/10.1063/1.1318766
    bruch = -1.876e-6 * from_cgs_to_si
    bruch2 = -1.929e-6 * from_cgs_to_si
    print("    1988 haftel+rebane<r^2>   ", "{:.7e}".format(haftel_rebane))
    print("    1970 mendelson            ", "{:.4e}".format(mendelson))
    print("    2000 bruch HF     cc-pV5Z ", "{:.3e}".format(bruch))
    print("    2000 bruch mpw1pw cc-pV5Z ", "{:.3e}".format(bruch2))


def print_H2_AQ():
    # H2 <r^2> values (0-vibrational state) from:
    # https://nur.nu.edu.kz/bitstream/handle/123456789/1095/kedziera_jcp_125_014318_2006.pdf;jsessionid=6054FB0418727EAB6667B8E76B976703?sequence=1
    #\cite{kedzieraDarwinMassvelocityRelativistic2006}
    kedziera = rebane_eq_34(
        au, p=2, N=4, q=-au.e, m=au.m_e, M=au.m_p,
        r2_1N=3.14539 * au.a0**2,
        r2_12=5.80533 * au.a0**2,
        r2_N1N=2.12705 * au.a0**2
    )
    # https://sci-hub.tw/https://aip.scitation.org/doi/10.1063/1.2978172
    # \cite{alexanderFullyNonadiabaticProperties2008}
    alexander = rebane_eq_34(
        au, p=2, N=4, q=-au.e, m=au.m_e, M=au.m_p,
        r2_1N=3.1465 * au.a0**2,
        r2_12=5.806 * au.a0**2,
        r2_N1N=2.1273 * au.a0**2
    )
    # \cite[p.~812]{jainDiamagneticSusceptibilityAnisotropy2007}
    jain = -3.99e-6 * from_cgs_to_si
    jain2 = -4.01e-6 * from_cgs_to_si
    # ON THE DIAMAGNETIC SUSCEPTIBILITY OF GASES
    # \cite{glickDIAMAGNETICSUSCEPTIBILITYGASES1961}
    glick = -4.04e-6 * from_cgs_to_si

    # TITLE:    A note on the magnetic susceptibility of hydrogen and its isotopomers
    # BIBTEX:   raynesNoteMagneticSusceptibility1974
    # URL:      https://www.sciencedirect.com/science/article/abs/pii/0009261474802368
    raynes_v0J0 = -1.7367 * raynes_units
    raynes_0K = -1.7057 * raynes_units
    raynes_300K = -1.7084 * raynes_units

    # TITLE:    Full CI calculations of the magnetizability and rotational g factor of the hydrogen molecule
    # BIBTEX:   ruudFullCICalculations1996
    # URL:      https://linkinghub.elsevier.com/retrieve/pii/S0166128096800364
    chi_ruud_1 = -67.13e-30 * si.µ_0 * si.N_A
    err_ruud_1 = 0.04e-30 * si.µ_0 * si.N_A
    chi_ruud_2 = -66.50e-30 * si.µ_0 * si.N_A
    err_ruud_2 = 0.03e-30 * si.µ_0 * si.N_A
    # Ruud's equation 1 gives some basic temperature dependence, assign T=0:
    X_x = -61.23 + (56.945590 * 6.08) / 4063.27 * 1/2
    X_z = -70.04 + (56.945590 * 0.53) / 4063.27 * 1/2
    ruud_eq1 = (X_x + 2*X_z)/3 * si.µ_0 * si.N_A

    # TITLE:    Magnetic Properties of the Hydrogen Molecules
    # BIBTEX:   ishiguroMagneticPropertiesHydrogen1954
    # URL:      https://link.aps.org/doi/10.1103/PhysRev.94.350
    ishiguro_direct = -4.0689e-6 * from_cgs_to_si

    print("In the paper")
    print("    1954 ishiguro 300K           {:.4e}".format(ishiguro_direct))
    print("    1974 raynes 0K               {:.4e}".format(raynes_0K))
    print("    2008 alexander + rebane<r^2> {:.4e}".format(alexander))
    print("    1996 ruud Eq.1 at 0K         {:.3e}".format(ruud_eq1))
    print("    1933 ruud ➔ havens 300K exp. {}".format(err_format(chi_ruud_2, err_ruud_2)))

    print("From Alpi's thesis")
    print("    1996 ruud 300K               {}".format(err_format(chi_ruud_1, err_ruud_1)))
    print("    2006 kedziera + rebane<r^2>  {:.5e}".format(kedziera))
    print("    2007 jain experimental       {:.2e}".format(jain))
    print("    2007 jain experimental       {:.2e}".format(jain2))
    print("    1961 glick                   {:.2e}".format(glick))
    print("    1974 raynes v=0 J=0,         {:.4e}".format(raynes_v0J0))
    print("    1974 raynes 300K             {:.4e}".format(raynes_300K))
    #% H2
    #% d ishiguro: from_cgs_to_si*-4.0689e-6=-5.1131305392766034e-11
    #% e raynes: -1.7057*raynes_units = -5.0932749430596263e-11
    #% f ruud: -67.13 * ruud_units = -5.080160268542567e-11 @ 300 K
    #% g rebane: -5.2065e-11 \cite{rebane} with \cite{alexanderRovibrationallyAveragedProperties2007}


def print_H2_BO():
    # TITLE:    Rovibrationally averaged properties of H2 using Monte Carlo methods
    # BIBTEX:   alexanderRovibrationallyAveragedProperties2007
    # URL:      https://onlinelibrary.wiley.com/doi/10.1002/qua.21130
    alexander_rebane = rebane_eq_34(
        au, p=2, N=4, q=-au.e, m=au.m_e, M=np.inf,
        r2_1N=3.037 * au.a0**2,  # e-p
        r2_12=5.635 * au.a0**2,  # e-e
        r2_N1N=1.40**2 * au.a0**2   # p-p  (constant in BO)
    )

    # TITLE:    Potential‐Energy Curves for the X1Σg+, b3Σu+, C1Πu States of the Hydrogen Molecule
    # BIBTEX:   kol/osPotentialEnergyCurves1965
    # URL:      https://aip.scitation.org/doi/abs/10.1063/1.1697142
    kolos_rebane = rebane_eq_34(
        au, p=2, N=4, q=-au.e, m=au.m_e, M=np.inf,
        r2_1N=3.0363543 * au.a0**2,  # e-p
        r2_12=5.6323895 * au.a0**2,  # e-e
        r2_N1N=1.40**2 * au.a0**2    # p-p (constant in BO)
    )

    # TITLE:    The hydrogen molecule H2 in inclined configuration in a weak magnetic field
    # BIBTEX:   alijahHydrogenMoleculeRmH2019a
    # URL:      https://arxiv.org/pdf/1903.08324.pdf
    # Use susceptibility values for inclinations, phi = [0, 45, 90] degrees
    alijah = np.array([-0.7647, -0.8258, -0.88700]) * au_magnz_to_SI
    # Reproduce susceptibilities from particle distances with http://arxiv.org/abs/1409.6204
    alijah_f = lambda p: -0.5*(0.7646*(1+np.cos(p)**2) + 1.00929*np.sin(p)**2)*au_magnz_to_SI

    # TITLE:    A note on the magnetic susceptibility of hydrogen and its isotopomers
    # BIBTEX:   raynesNoteMagneticSusceptibility1974
    # URL:      https://www.sciencedirect.com/science/article/abs/pii/0009261474802368
    raynes = [-1.457 * raynes_units, -1.677 * raynes_units]  # [parall, perpend]
    raynes_kolos = [-1.5234 * raynes_units, -1.7847 * raynes_units]

    # TITLE:    Magnetic Properties of the Hydrogen Molecules
    # BIBTEX:   ishiguroMagneticPropertiesHydrogen1954
    # URL:      https://link.aps.org/doi/10.1103/PhysRev.94.350
    # A function was given for angle
    ishiguro_f = lambda th: (-3.5814 + (-0.7339 + 0.1947) * np.sin(th)**2) * 1e-6*from_cgs_to_si
    ishiguro = ishiguro_f(np.array([0, np.pi/2]))
    # Integrate ishiguro_f in 2d plane, get constant 0.5.
    # This is incorrect as perpendicular is more likely!
    #  1/(pi/2) * int sin t ^2
    #= 1/(pi/2) * int 0.5*(1-cos2x)
    #=  1/(pi) * (x - 0.5*sin(2x))
    #=  1/(pi) * (((pi/2)) - 0)
    #=  1/(pi) * (pi/2)
    #=  (1/2)
    #ishiguro_integral = (-3.5814 + (-0.7339 + 0.1947) * (1/2)) * 1e-6*from_cgs_to_si

    # TITLE:    Full CI calculations of the magnetizability and rotational g factor of the hydrogen molecule
    # BIBTEX:   ruudFullCICalculations1996
    # URL:      https://linkinghub.elsevier.com/retrieve/pii/S0166128096800364
    ruud_units = si.µ_0 * si.N_A * 1e-30
    ruud_JT = -65.675 * ruud_units  # Fixed by JT


    # TITLE:    The Magnetic Properties of the Hydrogen Molecule
    # BIBTEX:   ludwigMagneticPropertiesHydrogen1969
    # URL:      https://www.degruyter.com/document/doi/10.1515/zna-1969-0335/html
    ludwig_JT = -5.037e-11

    # cybulski 1997
    # https://doi.org/10.1063/1.473123
    m1, d1 = -0.8321, 0.1047  # Mean, difference
    # cybulski 1997 citing sundholm 1996
    # https://aip.scitation.org/doi/10.1063/1.472905
    m2, d2 = -0.8311, 0.1042
    # Transform magnetic mean (m) and anisotrophy (d) to parallel and perpendicular
    # components using equations 4 and 5 in cybulski's paper.
    cybulski_mean = m1 * au_magnz_to_SI
    sundholm_mean = m2 * au_magnz_to_SI
    cybulski = np.array([m1 + 2/3 * d1, m1 - 1/3 * d1]) * au_magnz_to_SI
    cybulski_sundholm = np.array([m2 + 2/3 * d2, m2 - 1/3 * d2]) * au_magnz_to_SI


    print("These all are with R=1.40 (probably)")
    print("If there is star *, it is in the paper\n")
    print("Rotational average need not to be calculated:")
    print("   *{:<36} {:.3e}".format("2007 alexander + rebane<r^2>", alexander_rebane))
    print("   *{:<36} {:.4e}".format("1996 ruud", ruud_JT))
    print("   *{:<36} {:.3e}".format("1969 ludwig", ludwig_JT))
    print("   *{:<36} {:.4e}".format("1997 cybulski", cybulski_mean))
    print("    {:<36} {:.4e}".format("1965 kolos     + rebane<r^2>", kolos_rebane))
    print("    {:<36} {:.4e}".format("1996 cybulski ➔ sundholm", sundholm_mean))


    print("\nRotational average is processed with (1/3*‖ + 2/3*⟂)")

    pimcBO = rot_avg(-4.571e-11, -5.19e-11)
    print("   *{:<36} {:.4e}".format("2019 alijah", rot_avg(*alijah[[0,-1]])))
    #print(     {:<36} {:.6e}".format("2019 alijah 2 F", rot_avg(*alijah_f(np.array([0, np.pi/2])))))
    print("   *{:<36} {:.3e}".format("1974 raynes", rot_avg(*raynes)))
    print("   *{:<36} {:.4e}".format("1954 ishiguro", rot_avg(*ishiguro)))
    print("    {:<36} {:.4e}".format("1965 raynes ➔ kolos", rot_avg(*raynes_kolos)))
    print("    {:<36} {}".format("     pimc BO fixed T=3000K", err_format(pimcBO, 0.03e-11)))

    print("\nAnisotropic susceptibilities (‖, ⟂)")
    print("         pimc3 BO fixed      (-4.571(14)e-11, -5.19(2)e-11)")
    print("    2019 alijah              ({:.5e},   {:.5e})".format(*alijah[[0,-1]]))
    print("    1974 raynes              ({:.5e},   {:.5e})".format(*raynes))
    print("    1965 raynes&kolos        ({:.5e},   {:.5e})".format(*raynes_kolos))
    print("    1954 ishiguro            ({:.5e},   {:.5e})".format(*ishiguro))
    print("    1997 cybulski            ({:.5e},   {:.5e})".format(*cybulski))
    print("    1996 cybulski ➔ sundholm ({:.5e},   {:.5e})".format(*cybulski_sundholm))


    #% raynes_units = 2.9860320941898496e-11
    #% au_magnz_to_SI=5.97165254931149e-11
    #% from_cgs_to_si=1.2566370614359172e-05
    #% ruud_units = 7.567645268199863e-13

    # Problems below:
    # Alijah numbers taken from wrong column
    # ishiguro integral evaluation to 0.5 is wrong, as it is in 2d

    #% f ludwig:
    #% g alijah: (2*-0.76465-1.00929)/3*au_magnz_to_SI = -5.053192481718886e-11
    #% i ishiguro: from_cgs_to_si*(-3.5814-0.5*0.7339+0.5*0.1947)*1e-6=-4.8393093235897166e-11
    #% j raynes: (-1.457+2*-1.677)/3*raynes_units = -4.788600135049122e-11
    #% k raynes w/ KW: (-1.5234+2*-1.7847)/3*raynes_units = -5.0690880830966895e-11
    #% l rebane: -5.0699 \cite{rebane} with \cite{alexanderRovibrationallyAveragedProperties2007}
    #% m ruud: -65.675 * ruud_units = -4.970051029890259e-11

def print_HD():

    # TITLE:    A note on the magnetic susceptibility of hydrogen and its isotopomers
    # BIBTEX:   raynesNoteMagneticSusceptibility1974
    # URL:      https://www.sciencedirect.com/science/article/abs/pii/0009261474802368

    print("In paper")
    print("    raynes 0K  ", "{:.3e}".format(-1.7022 * raynes_units))
    print("From my thesis")
    print("    raynes 300K", "{:.3e}".format(-1.7050 * raynes_units))

    #% HD
    #% e raynes: -1.7022*raynes_units = -5.082823830729962e-11


def print_D2():
    # D2 <r^2> values (0-vibrational state) from:
    # TITLE:    Accurate non-Born–Oppenheimer calculations of the lowest vibrational energies of D2 and T2 with including relativistic corrections
    # BIBTEX:   bubinAccurateNonBornOppenheimer2010
    # URL:      https://linkinghub.elsevier.com/retrieve/pii/S0009261410007554
    bubin_rebane = rebane_eq_34(
        au, p=2, N=4, q=-au.e, m=au.m_e, M=3670.4829654*au.m_e,
        r2_1N=3.113364 * au.a0**2,
        r2_12=5.754556 * au.a0**2,
        r2_N1N=2.077687 * au.a0**2
    )
    # \cite{ishiguroMagneticPropertiesHydrogen1954}

    # TITLE:    Magnetic Properties of the Hydrogen Molecules
    # BIBTEX:   ishiguroMagneticPropertiesHydrogen1954
    # URL:      https://link.aps.org/doi/10.1103/PhysRev.94.350

    # TITLE:    A note on the magnetic susceptibility of hydrogen and its isotopomers
    # BIBTEX:   raynesNoteMagneticSusceptibility1974
    # URL:      https://www.sciencedirect.com/science/article/abs/pii/0009261474802368

    print("In paper")
    print("    1954 ishiguro            ", "{:.4e}".format(-4.0306e-6 * from_cgs_to_si))
    print("    1974 raynes 0K           ", "{:.3e}".format(-1.6984 * raynes_units))
    print("From my thesis")
    print("    2010 bubin + rebane<r^2> ", "{:.6e}".format(bubin_rebane))
    print("    1974 raynes 300K         ", "{:.3e}".format(-1.7004 * raynes_units))


    #% D2
    #% d ishiguro: -4.0306e-6*from_cgs_to_si = 5.065001339823607e-11
    #% e raynes: -1.6984*raynes_units = -5.07147690877204e-11
    #% h rebane: -5.164286e-11 from <r²> from bubin


def print_T2():
    # https://www.sciencedirect.com/science/article/pii/S0009261410007554
    # \cite{bubinAccurateNonBornOppenheimer2010}
    chi_T2 = rebane_eq_34(
        au, p=2, N=4, q=-au.e, m=au.m_e, M=5496.92158*au.m_e,
        r2_1N=3.099443 * au.a0**2,
        r2_12=5.732488 * au.a0**2,
        r2_N1N=2.056241 * au.a0**2
    )
    print("    bubin + rebane<r^2> ", "{:.6e}".format(chi_T2))


def print_Ps2():
    chi_Ps2_AQ_ref_rebane =  rebane_E_to_susc(5.839075)

    # Ps2 <r^2> values from rebane himself
    # TITLE:    Nonadiabatic theory of diamagnetic susceptibility of molecules
    # BIBTEX:   rebaneNonadiabaticTheoryDiamagnetic2002
    # LINK:     http://link.springer.com/10.1134/1.1503752
    chi_Ps2_AQ_r2_rebane = rebane_eq_34(
        au, p=2, N=4, q=-au.e, m=au.m_e, M=au.m_e,
        r2_1N=29.1093 * au.a0**2,
        r2_12=46.3683 * au.a0**2,
        r2_N1N=46.3683 * au.a0**2
    )


    # TITLE:    Relativistic corrections to the ground-state energy of the positronium molecule
    # BIBTEX:   bubinRelativisticCorrectionsGroundstate2007
    # URL:      https://link.aps.org/doi/10.1103/PhysRevA.75.062504
    chi_Ps2_AQ = rebane_eq_34(
        au, p=2, N=4, q=-au.e, m=au.m_e, M=au.m_e,
        r2_1N=29.11270462 * au.a0**2,
        r2_12=46.37487912 * au.a0**2,
        r2_N1N=46.37487912 * au.a0**2
    )

    print("    rebane only         ", "{:.5e}".format(chi_Ps2_AQ_ref_rebane))
    print("    rebane only         ", "{:.5e}".format(chi_Ps2_AQ_r2_rebane))
    print("    bubin + rebane<r^2> ", "{:.8e}".format(chi_Ps2_AQ))


def print_Ps():
    chi_Ps = H_susceptibility(si, si.m_e, si.m_e)
    chi_Ps_pimc = rebane_eq_34(    # See data -1 at the end of this file for <r^2> data
        au, p=1, N=2, q=-au.e, m=au.m_e, M=au.m_e,
        r2_1N=12.00,    # p-e
        r2_12=0.0,
        r2_N1N=0.0
    )
    print("    analytic <r^2> + rebane    ", "{:.6e}".format(chi_Ps))
    print("    pimc + rebane<r^2>         ", "{:.6e}".format(chi_Ps_pimc))



def susceptibility_reference_values():
    """Calculates all susceptibility reference values"""
    print("Susceptibility reference values in All Quantum (AQ) and Born Oppenheimer (BO) systems\n")
    print("Notation '____ + rebane<r^2>' means that numeric <r^2> value is taken from source and ")
    print("applied to rebane's eq. 34.")

    print("\n---------------------\nH\n")
    print_H()
    print("\n---------------------\nHe\n")
    print_He_BO()
    print("\n---------------------\nH2 BO \n")
    print_H2_BO()
    print("\n---------------------\nH2 AQ \n")
    print_H2_AQ()
    print("\n---------------------\nHD\n")
    print_HD()
    print("\n---------------------\nD2 \n")
    print_D2()
    print("\n---------------------\nT2 \n")
    print_T2()
    print("\n---------------------\nPs2\n")
    print_Ps2()
    print("\n---------------------\nPs \n")
    print_Ps()
    print("")



def print_energy_of_H():
    """
    See Atkins, Molecular quantum mechanics 5th ed. p.88, eq. 3.44
    """
    Z = 1
    E = lambda n, m, c: -((Z**2 * m * c.e**4) / (32 * np.pi**2 * c.eps0**2 * c.hbar**2)) * 1/n**2

    print("    E_H AQ au   ", E(1, m_r(au.m_e, au.m_p), au))
    print("    E_H BO au   ", E(1, au.m_e, au))
    print("    E_Ps AQ au  ", E(1, au.m_e/2, au))


def main():
    print("From a.u. to SI conversion factor:")
    print("    magnetizatibilty:", au_magnz_to_SI)
    print("    susceptibility: ", from_au_to_SI_real)
    print("--------------------")
    print("--------------------\n")
    print("")

    print("All calculations assume T=0K, ground state\n")
    print("--------------------")
    susceptibility_reference_values()

    print("--------------------")
    print("--------------------\n")
    print("Analytical energies of few systems:")
    print_energy_of_H()

main()
