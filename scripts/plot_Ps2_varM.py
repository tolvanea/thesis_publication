# Plot dinuclear molecule with varying nuclear mass. The figure includes Ps2, H2, and D2
import numpy as np
import matplotlib.pyplot as plt
import uncertainties as unp

from tools import extrapolation
from tools import plotting
from tools.plotting import dot, dash, xxx
from tools import printing
from tools import reading


def get_Ps2_varM_T300_data_at_tau0():
    # Concatenate two calculations into one.
    # The second calculation adds nuclear masses m0.00108 and m0.00217
    # I can not combine the json files directly, because they have different
    # time step. Damnit!
    system1 = reading.load_system("datas/Ps2_varM_T300/susceptibility_data.json")
    system1.add_tau0_extrapolation()

    system2 = reading.load_system("datas/Ps2_varM_T300_new_datapoints/susceptibility_data.json")
    system2.add_tau0_extrapolation()
    assert(len(system2.range2) == 2) ## two data points of masses

    combined_masses =  np.zeros(len(system1.range2) + 2)
    combined_data_tau0 = combined_masses.copy()
    combined_err_tau0 = combined_masses.copy()

    vectors = [
        (system1.range2,    system2.range2,     combined_masses),
        (system1.data_tau0, system2.data_tau0,  combined_data_tau0),
        (system1.err_tau0,  system2.err_tau0,   combined_err_tau0),
    ]

    for v1, v2, combined in vectors:
        combined[:] = np.hstack((v1[0], v2[0], v1[1], v2[1], v1[2:]))

    # Also, because Ps2 T=300 is calculated more accurately, use that value instead
    system3 = reading.load_system("datas/Ps2_varT/susceptibility_data.json")
    system3.add_tau0_extrapolation()
    combined_data_tau0[0] = system3.data_tau0[0]
    combined_err_tau0[0] = system3.err_tau0[0]

    return combined_masses, combined_data_tau0, combined_err_tau0

def get_Ps2_varM_T1000_data_at_tau0():
    system = reading.load_system("datas/Ps2_varM_T1000/susceptibility_data.json")
    system.add_tau0_extrapolation()
    return system.range2, system.data_tau0, system.err_tau0

def get_Ps2_varM_T3000_data_at_tau0():
    system = reading.load_system("datas/Ps2_varM_T3000/susceptibility_data.json")
    system.add_tau0_extrapolation()
    return system.range2, system.data_tau0, system.err_tau0

def get_H2_BO_at_tau0():
    system = reading.load_system("datas/H2_varT_BO/susceptibility_data.json")
    system.add_tau0_extrapolation()
    return system.data_tau0, system.err_tau0
    #return chi_T300, err_T300, chi_T1000, err_T1000

def append_BO(calculation, BO_susc, BO_err):
    m, susc, err = calculation
    m = np.append(m, 1e48)
    susc= np.append(susc, BO_susc)
    err = np.append(err, BO_err)
    return m, susc, err


def plot():
    susc_BO, err_BO = get_H2_BO_at_tau0()
    BO_T1000 = susc_BO[1]

    T300  = get_Ps2_varM_T300_data_at_tau0()
    T1000 = get_Ps2_varM_T1000_data_at_tau0()
    T3000 = get_Ps2_varM_T3000_data_at_tau0()
    if True:  # include BO calculation
        m_T300,  susc_T300,  err_T300  = append_BO(T300,  susc_BO[0], err_BO[0])
        m_T1000, susc_T1000, err_T1000 = append_BO(T1000, susc_BO[1], err_BO[1])
        m_T3000, susc_T3000, err_T3000 = append_BO(T3000, susc_BO[2], err_BO[2])
    else:
        m_T300,  susc_T300,  err_T300  = T300
        m_T1000, susc_T1000, err_T1000 = T1000
        m_T3000, susc_T3000, err_T3000 = T3000

    # constant_term determines d in fit y = a/x^3 + b/x^2 + c/x + d
    # constant_term=False means that corresponding BO values are used, and
    # constant_term=True means that it is automatically searched
    constant_term = False

    # This offsets only the logarimic plot, but does not affect fits. Can be set to X_inf
    X_inf_plot = -4.96e-11 # this is BO_T300 value

    # Switch between different fits
    # switch = 0:   y = a/x^3 + b/x^2 + c/x + d                  # 'Meh' fit
    # switch = 1:   y = a/x^2 + b/x + c/x^(1/2) + d/x^(1/16) + e # Best fit
    # switch = 2:   y = a*x^b + c                                # Bad fit
    # switch = 3:   y = a*e^(b*x) + c                            # Bad fit
    switch = 1
    if switch <= 1: # fit y = a/x^3 + b/x^2 + c/x + d
        if switch==1:
            powers = [2, 1, 1/2, 1/16]
        else:
            powers = None
        log_y = True
        fit_with_err = lambda *args: extrapolation.inverse_polynomial_errorbardata_fit(
            *args, constant_term=constant_term, log_y=log_y, powers=powers
        )
        fit_teach = lambda *args: extrapolation.teach_inverse_polynomial(
            *args, constant_term=constant_term, log_y=log_y, powers=powers
        )
        fit_predict = lambda *args: extrapolation.predict_inverse_polynomial(
            *args, constant_term=constant_term, powers=powers
        )
    else: # Exponential fits  a*x^b + c  or  a*e^(b*x) + c. Theese are crap fits.
        # Automatically finds constant c regardless of variable 'constant_term'
        low = BO_T1000 * 0.9 - BO_T1000
        high = BO_T1000 * 1.1 - BO_T1000
        if switch == 2: # a*x^b + c
            fit_with_err = lambda *args: extrapolation.power_coeff_errorbardata_fit(
                *args, c_lower_bound=low, c_upper_bound=high
            )
            fit_teach = lambda *args: extrapolation.teach_power_coefficient(
                *args, c_lower_bound=low, c_upper_bound=high
            )
            fit_predict = lambda *args: extrapolation.predict_power_exponent(*args)
        if switch == 3: # a*e^(b*x) + c
            fit_with_err = lambda *args: extrapolation.exponential_errorbardata_fit(
                *args, c_lower_bound=low, c_upper_bound=high
            )
            fit_teach = lambda *args: extrapolation.teach_exponential(
                *args, c_lower_bound=low, c_upper_bound=high
            )
            fit_predict = lambda *args: extrapolation.predict_exponential(*args)


    ax, fig = plotting.create_figure_axis("Ps2_varM")
    ax.set_xlabel("$M$ (units of $m_\\mathrm{p}$)")
    ax.set_ylabel("$\\chi_\\infty - \\chi \\; (\\frac{\\mathrm{m}^3}{\\mathrm{mol}})$", rotation='horizontal')
    ax.yaxis.set_label_coords(-0.03,1.03)
    fig.subplots_adjust(left=0.15)  # Adjust to get more x-ticks


    # Btw, fitting is best to T=1000K data. Fits at temperatures 300K and 3000K are crap
    plots = [
        ("$T=300\\;K$",  m_T300,  susc_T300,  err_T300,  "C0", dot),
        ("$T=1000\\;K$", m_T1000, susc_T1000, err_T1000, "C1", dash),
        ("$T=3000\\;K$", m_T3000, susc_T3000, err_T3000, "C2", xxx),
    ]

    error_boundaries = False # enable this to include error boundaries (they are useless in this plot)

    for (label, m, X, err, color, marker) in plots:
        print("   ",label)

        if not constant_term:
            X_inf = X[-1]*0.999
        else:
            X_inf=0.0

        # Print bare errorbars of data
        line, caps, bars = ax.errorbar(m, -X + X_inf_plot, err, label=label, ls="", c=color, capsize=4, **marker)
        #plotting.set_errobar_fake_transparency(line, caps, bars, alpha=0.1)

        # Fit curve to errorbars
        m_range = np.geomspace(m[0], m[-1], 1001)
        if error_boundaries:
            fit, fit_err = fit_with_err(m, X-X_inf, err, m_range,)
            fit = fit + X_inf

            ax.plot(m_range, -fit+X_inf_plot - fit_err , ls=":", c=color)
            ax.plot(m_range, -fit+X_inf_plot + fit_err, ls=":", c=color)
        else:
            model = fit_teach(m, X-X_inf)
            fit = fit_predict(m_range, model)
            fit = fit + X_inf

        ax.plot(m_range, -fit+X_inf_plot, ls="--", c=color)


    # Plot gray legends
    if error_boundaries:
        line0, caps0, bars0 = ax.errorbar(
            [np.nan], [np.nan], [0.0], ls="--", c="gray", label="fit"
        )
        plotting.hide_error_bar_with_fake_transparency(caps0, bars0)
        line0, caps0, bars0 = ax.errorbar(
            [np.nan], [np.nan], [0.0], ls=":", c="gray", label="$1\\sigma$ boundary"
        )
        plotting.hide_error_bar_with_fake_transparency(caps0, bars0)

    if False: # References disabled due to clutter
        refdata = [
            # x,    y,                         label,                color, marker            size
            ( 1.0,  5.1131e-11  + X_inf_plot, ("ref. H$_2$ $0\\;$K", "grey", "o",              5)), # AQ, Index 0
            #(10.0, 4.952e-11 + X_inf_plot,   ("ref. BO $0\\;$K",    "grey", "$\\rightarrow$", 15)),
            #(2.0,  5.16429e-11 + X_inf_plot, ("ref. D$_2$ $0\\;$K", "grey", "^",              None)),
            #(10.0, -BO_T300 + X_inf_plot,    ("BO $300\\;$K",               "C0", "$\\rightarrow$", 15)),
            #(10.0, -BO_T1000 + X_inf_plot,   ("BO $1000\\;$K",              "C1", "$\\rightarrow$", 15)),
            #    x,          y,              index above
            (  1.0, 5.0933e-11 + X_inf_plot, 0), # H2 AQ, the '0' means index
            (  1.0,   5.08e-11 + X_inf_plot, 0),
            (  1.0,   5.04e-11 + X_inf_plot, 0),
            (  1.0,   5.01e-11 + X_inf_plot, 0),
        ]
        plotting.plot_references(ax, refdata)
    ax.legend()

    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlim((3e-4, 20))
    ax.set_ylim((1.4e-13, 7e-10))


def main():
    plot()

    plt.show()

if __name__ == "__main__":
    main()


