# Plot figure with Ps and Ps2 with variating temperature
import numpy as np
import matplotlib.pyplot as plt

from tools import extrapolation
from tools.extrapolation import linear_errorbardata_fit, proper_mean_with_errorbars
from tools import plotting
from tools.plotting import dot, dash
from tools import reading
from tools.printing import err_format


def Ps_data():
    """
    Data for T=100 is missing, use value from my MSc thesis
    """
    system = reading.load_system("datas/Ps_varT/susceptibility_data.json")
    system.add_tau0_extrapolation()
    T = np.append([100.0], system.range2)
    susc = np.append([-2.377e-10], system.data_tau0)
    err = np.append([0.006e-10/2], system.err_tau0)
    return T, susc, err

def Ps2_data():
    """
    Use recalculated Ps2 (dating 2021-10) that Juha asked
    """
    system = reading.load_system("datas/Ps2_varT/susceptibility_data.json")
    system.add_tau0_extrapolation()
    T = system.range2
    susc = system.data_tau0
    err = system.err_tau0
    return T, susc, err

def Ps2_data_old_calculation():
    system = reading.load_system("datas/Ps2_varT_old_calculation/susceptibility_data.json")
    if True:
        system.range2 = system.range2[1:]   # Drop T=30 because it is inaccurate
        system.data = system.data[:, 1:]  # Drop T=30
        system.err = system.err[:, 1:]    # Drop T=30
    else:
        system.range2 = system.range2[1:]   # Drop T=30 because it is inaccurate
        system.range1 = system.range1[:-1]  # Drop tau=0.05 because for nonlinear error
        system.data = system.data[:-1, 1:]  # Drop tau=0.05 and T=30
        system.err = system.err[:-1, 1:]    # Drop tau=0.05 and T=30

    system.add_tau0_extrapolation()
    T = system.range2
    susc = system.data_tau0
    err = system.err_tau0
    return T, susc, err

def Ps2_data_combination():
    """ Nah, not that good to combine: the original calculation (1) is quite bad due to too long time step use"""
    T, susc1, err1 = Ps2_data_old_calculation()
    _, susc2, err2 = Ps2_data()

    susc3, err3 = proper_mean_with_errorbars((susc1, susc2), (err1, err2))
    return T, susc3, err3


def main(split_axis=False):
    errorbar_curves = False  # Plot confifence intervals
    print("Ps, Ps2")

    T_Ps, susc_Ps, err_Ps = Ps_data()
    #T_Ps2, susc_Ps2, err_Ps2 = Ps2_data()
    #_, susc_Ps2_old, err_Ps2_old = Ps2_data_old_calculation()  # OLD, worse values
    T_Ps2, susc_Ps2, err_Ps2 = Ps2_data_combination()

    if split_axis: # Split y axis in two parts
        fig, ax_Ps, ax_Ps2 = plotting.split_axes(
            xlim=(-100, 2000),
            ylim1=(2.235e-10, 2.415e-10),
            ylim2=(4.880e-10, 4.975e-10),
            fig_name="Ps_Ps2_split",
            ax_ratio=2/3
        )
        ax_Ps.set_xlabel("$T$ $(\\mathrm{{K}})$")
        ax_Ps2.set_ylabel("$-\\chi \\; (10^{-10} \\frac{\\mathrm{m}^3}{\\mathrm{mol}})$", rotation='horizontal')
        fig.subplots_adjust(bottom=0.15, left=0.16) # give room for x,y-labels
    else: # Scale y-value of the second curve so that they fit in on figure
        susc_Ps2, err_Ps2 = susc_Ps2/2, err_Ps2/2
        #susc_Ps2_old, err_Ps2_old = susc_Ps2_old/2, err_Ps2_old/2
        #susc_Ps2_mean, err_Ps2_mean = susc_Ps2_mean/2, err_Ps2_mean/2

        ax, fig = plotting.create_figure_axis("Ps_Ps2")
        ax.set_xlim((-90, 1700))
        ax.set_ylim((2.27, 2.4800))

        ax.set_ylabel(
            r"$-\chi/N  \; (10^{-10} \frac{\mathrm{m}^3}{\mathrm{mol}})$",
            rotation='horizontal'
        )
        ax.yaxis.set_label_coords(0.00,1.03)
        ax_Ps, ax_Ps2 = ax, ax

    plots = [
        ("Ps$_2$", T_Ps2, susc_Ps2, err_Ps2, "C1", dot, ax_Ps2),
        ("Ps",     T_Ps,  susc_Ps,  err_Ps,  "C0", dash, ax_Ps),
        #("Ps$_2$ old", T_Ps2, susc_Ps2_old, err_Ps2_old, "C2", dot, ax_Ps2),
        #("Ps$_2$ new+old", T_Ps2, susc_Ps2_mean, err_Ps2_mean, "C3", dot, ax_Ps2),
    ]

    for (label, T, chi, err, color, marker, ax) in plots:
        print("   ",label)

        # Print bare errorbars of data
        line, caps, bars = ax.errorbar(T, -1e10*chi, 1e10*err, label=label, ls="", c=color, capsize=4, **marker)
        #plotting.set_errobar_fake_transparency(line, caps, bars, alpha=0.1)

        # Fit curve to errorbars
        T_range = np.linspace(0.0, T[-1], 41)
        fit, fit_err = linear_errorbardata_fit(T, chi, err, T_range)
        print(" "*7, "At T=0:", err_format(fit[0], err[0]))

        # Plot T=0 errorbar with ligher color
        line0, caps0, bars0 = ax.errorbar(T_range[0], -1e10*fit[0], 1e10*fit_err[0], ls="", c=color, capsize=4, **marker)
        plotting.set_errobar_fake_transparency(line0, caps0, bars0, alpha=0.4)

        ax.plot(T_range, -1e10*fit, ls="--", c=color)

        sigma1_pvalue = 1-0.682689
        #sigma2_pvalue = 1-0.954499
        a, b, a_err, b_err, f = extrapolation.line_fit_confidense_intervals(
            T, chi, err, p_value=sigma1_pvalue
        )
        double = int(not split_axis and label=="Ps$_2$") + 1
        aa, bb = err_format(double*a, double*a_err), err_format(double*b, double*b_err)
        print(" "*7, "Params 'y=ax+b'   a:", aa, ", b:", bb)
        if errorbar_curves:
            ax.plot(T_range, -1e10*fit - 1e10*fit_err, ls=":", c=color)
            ax.plot(T_range, -1e10*fit + 1e10*fit_err, ls=":", c=color)

            if False: # primitive error boundaries from a_err and b_err
                a, b, ae, be, _ = extrapolation.line_fit_confidense_intervals(
                    T-T.mean(), chi, err, p_value=sigma1_pvalue
                )
                ah, al, bh, bl = a+ae, a-ae, b+be, b-be  # Highs and lows
                x = T_range-T.mean() # shifted
                low = np.minimum(np.minimum(ah*x + b, al*x + b), a*x + bl)
                hig = np.maximum(np.maximum(ah*x + b, al*x + b), a*x + bh)
                ax.plot(T_range, -1e10*low, ls=":", c="black")
                ax.plot(T_range, -1e10*hig, ls=":", c="black")
        if label=="Ps$_2$":
            print("")
            print(" "*7, "Susceptibility from combined data: (These values are used in paper)")
            print(" "*7, "T=0K           100K           300K             500K")
            ss = [fit[0]] + list(chi)
            es = [fit_err[0]] + list(err)
            print(" "*7, ", ".join([err_format(double*s, double*e) for (s, e) in zip(ss, es)]))
            print("")


    if split_axis:
        # Add legend "Ps_2"
        ax_Ps.errorbar([0], [0], [0], label=plots[1][0], ls="", c=plots[1][4], capsize=4)

    # Plot gray legends
    if errorbar_curves:
        line0, caps0, bars0 = ax_Ps.errorbar(
            [np.nan], [np.nan], [0.0], ls="--", c="gray", label="fit"
        )
        plotting.hide_error_bar_with_fake_transparency(caps0, bars0)
        line0, caps0, bars0 = ax_Ps.errorbar(
            [np.nan], [np.nan], [0.0], ls=":", c="gray", label="$1\\sigma$ conf.interval"
        )
        plotting.hide_error_bar_with_fake_transparency(caps0, bars0)

    refdata_Ps = (
    #    x,   y,            (label,     color, marker, size)
        (0.0, 2.388663, ("Reference (Ps)", "C0", "x", 7)),
    )
    plotting.plot_references(ax_Ps, refdata_Ps)
    ax_Ps.legend()

if __name__ == "__main__":
    main(split_axis=False)
    plt.show()
