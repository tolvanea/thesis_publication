from uncertainties import ufloat
import numpy as np


def print_spaced(label, vec, err=None):
    print("{:<16}".format(label), end="")
    if err is not None:
        assert (len(vec) == len(err))
        for val, e in zip(vec, err):
            s = "{:+.2uS}".format(ufloat(val, e))
            s = "{:>16}".format(s)
            print(s, end="")
    else:
        for val in vec:
            s = "{:>16}".format(val)
            print(s, end="")
    print("")


def print_and_write_tables(system, write_file, title, identation="    "):

    lines = []
    write_data_table(system, lines, title=title, identation=identation)

    if write_file is not None:
        with open(write_file, 'w') as f:
            for line in lines:
                f.write(line + "\n")
                print(line)
    else:
        for line in lines:
            print(line)


def write_data_table(system, lines, title="", identation="    "):
    """
    Writes output to parameter list 'lines' like:
    ```
    print_obs(), obsname=Et
    \tau  T|T300.000  |T1000.0   |
    -------------------------------
    tau0.01|-1.173(12)|-1.158(9) |
    tau0.02|-1.173(12)|-1.158(9) |
    ```
    """
    assert(system.data.shape[0] == len(system.range1))  # please clean tau0 row out of data matrix
    lines.append(title)
    labels1, labels2 = generate_names_for_calculations(system)

    # Add tau=0 extrapolation
    sym_name = system.latex_symbol1.replace("\\","")
    range1_labels = [sym_name + str(0.0)] + labels1
    range2_labels = labels2
    data = np.vstack((system.data_tau0, system.data))
    err = np.vstack((system.err_tau0, system.err))

    # Add T=0 extrapolation, if available
    if hasattr(system, "data_tau0_T0"):
        sym_name = system.latex_symbol2.replace("\\","")
        range2_labels = [sym_name + str(0.0)] + labels2
        T0_data = np.zeros(data.shape[0])
        T0_data[0] = system.data_tau0_T0[0]
        T0_err = np.zeros(data.shape[0])
        T0_err[0] = system.err_tau0_T0[0]
        T = lambda v: np.atleast_2d(v).T  # transpose 1d vector
        data = np.hstack((T(T0_data), data))
        err = np.hstack((T(T0_err), err))


    # matrix of results as formatted strings
    str_mat = [[None for j in range(data.shape[1])] for i in range(err.shape[0])]

    max_cell_width = 0  # maxiumum cell size
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            if (not np.isfinite(data[i, j])) or data[i, j] == 0.0:
                cell = " " * 6
            else:
                cell = err_format(data[i, j], err[i, j])  # 1-sigma errors
            str_mat[i][j] = cell
            max_cell_width = max(len(cell), max_cell_width)

    leftmost_column_width, column_width = write_first_row(
        system, lines, range1_labels, range2_labels,
        max_width_of_cell=max_cell_width, identation=identation,
    )

    for i in range(data.shape[0]):
        line = []
        for j in range(-1, data.shape[1]):

            if j == -1:
                row_prefix = "{:{w}}|".format(range1_labels[i],
                                              w=leftmost_column_width)
                line.append(row_prefix)

            else:
                if data[i, j] is not None:
                    val = str_mat[i][j]
                    cell = "{:{w}}|".format(val, w=column_width)
                    line.append(cell)
                else:
                    line.append(" " * column_width + "|")

        lines.append(identation + "".join(line))

    lines.append("")


def write_first_row(system, lines, labels1, labels2, max_width_of_cell=15, identation="    "):
    """
    This writes first row in table to 'lines'. It may look like:
        \tau  T|T300.0        |T1000.0       |T3000.0       |
    :return:
        first column width
        non-first column width
    """
    longest_label1 = -1
    longest_label2 = -1
    for label1 in labels1:
        longest_label1 = max(len(label1), longest_label1)
    for label2 in labels2:
        longest_label2 = max(len(label2), longest_label2)
    leftmost_column_width = max(longest_label1,
                                len(system.latex_symbol1) + len(system.latex_symbol2) + 1)
    space = leftmost_column_width - len(system.latex_symbol1) - len(system.latex_symbol2)
    column_width = max(longest_label2 + 1, max_width_of_cell)

    first_row = []
    for j in range(-1, len(labels2)):
        if j == -1:
            upper_corner = "{:<}{:<{w}}{:>}|".format(system.latex_symbol1,
                                                     " ",
                                                     system.latex_symbol2,
                                                     w=space)
            first_row.append(upper_corner)
        else:
            first_row.append(("{:" + str(column_width) + "}|").format(labels2[j]))

    line = "".join(first_row)
    lines.append(identation + line)
    lines.append(identation + "-"*len(line))

    return leftmost_column_width, column_width


import string
class ShorthandFormatter(string.Formatter):
    """ Format uncertainties better.
    https://pythonhosted.org/uncertainties/user_guide.html
    """

    def format_field(self, value, format_spec):
        from uncertainties import UFloat
        if isinstance(value, UFloat):
            return value.format(format_spec + 'S')  # Shorthand option added
        # Special formatting for other types can be added here (floats, etc.)
        else:
            # Usual formatting:
            return super(ShorthandFormatter, self).format_field(
                value, format_spec)

def err_format(mu, sigma):
    """Format uncertainties number in form of 1.23(45) instead of 1.2345+/-0.0045."""

    frmtr = ShorthandFormatter()
    return frmtr.format("{0:u}", ufloat(mu, sigma))

def generate_names_for_calculations(system):
    """
    For example, if temperature and time step is variated, names are like
    "T100" and "tau0.01".
    """

    dir1_base_name = system.latex_symbol1.replace("\\","")
    dir2_base_name = system.latex_symbol2.replace("\\","")
    range1 = system.range1
    range2 = system.range2

    labels = [[], []]

    max_char_limit = 7
    # number formatting in string with constant length
    for i, (rang, base) in enumerate([(range1, dir1_base_name),
                                      (range2, dir2_base_name)]):
        for j, num in enumerate(rang):
            num_as_str = str(num)
            if len(num_as_str) > max_char_limit:
                num_as_str = num_as_str[:max_char_limit]

            labels[i].append(base + num_as_str)

    return labels[0], labels[1]

