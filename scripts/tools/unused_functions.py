import pickle
import json
import copy

import numpy as np

from tools import extrapolation
from tools import reading
from tools import printing

def read_cached_value(folder, estimator):
    """ Read data from cache-container of my original calculations
    This function is not used anymore as the data is read from json file.
    """
    import importlib.util


    # Import conf.py from string path
    path = folder + "/configure_instance.py"
    conf_module = importlib.import_module(path.replace("/", ".")[:-3])
    conf = conf_module.InitialSetup()

    if estimator == "susceptibility":
        filename = "∕MAGN∕dia.susc._(qA)^2_avg.pickle"
        # '_ext' means that it includes tau=0 extrapolate
        mean_name = "susc_ext"
        err_name = "susc_err_ext"
    elif estimator == "energy":
        filename = "∕E∕Vt.pickle"
        mean_name = "mean_extrapolation_at_range1=0"
        err_name = "err_extrapolation_at_range1=0"

    with open(folder + "/cache/" + filename, "rb") as f:
        p = pickle.load(f)
        mean_arr = p[mean_name]
        mean_err_arr = p[err_name]  # errors are 1sigma here

    assert(mean_arr.shape[1] == len(conf.range2))

    system = conf
    system.data = mean_arr[1:,:].astype(np.float64)
    system.err = mean_err_arr[1:,:].astype(np.float64)

    y_ext0 = []
    err_ext0 = []
    for i in range(system.data.shape[1]):
        y_at_0, err_at_0 = extrapolation.linear_errorbardata_fit(
            np.array(system.range1).astype(np.float64), system.data[:,i], system.err[:,i], 0.0
        )
        y_ext0.append(y_at_0)
        err_ext0.append(err_at_0)
        if np.abs(mean_arr[0,i] - y_at_0) > np.abs(y_at_0)*0.01:
            print("mean", folder + "/cache/" + filename)
            print(i, conf.range2[i], mean_arr[0,i], y_at_0)
        if np.abs(mean_err_arr[0,i] - err_at_0) > np.abs(err_at_0)*0.2:
            print("Err", folder + "/cache/" + filename)
            print(i, conf.range2[i], mean_err_arr[0,i], err_at_0)

    system.data_tau0 = np.array(y_ext0).astype(np.float64)
    system.err_tau0 = np.array(err_ext0).astype(np.float64)

    return system

def save_data(system, write_file):
    """
    Save data to json file, where system is from my original simulations. Change some names from
    original systemiguration.

    This function is not needed anymore as the json-files are saved already once with this function
    """

    labels1, labels2 = printing.generate_names_for_calculations(system)
    range1, range2 = system.range1, system.range2

    system = copy.deepcopy(system)
    setattr(system, "help1",
        "'range1' gives timesteps, and 'range2' gives to temperatures / nuclear masses"
    )
    setattr(system, "help2",
        "Columns of 'data'-matrix correspond to 'range1' and rows correspond to 'range2'."
    )
    setattr(system, "help3", "Errorsbars of 'err' are with 1-sigma.")
    setattr(system, "help4", "See '*_tables.txt' for better visualization of 'data' and 'err'.")
    setattr(system, "help5",
        "During runtime, extrapolation at tau=0 will be attached with 'data_tau0' and 'err_tau0'."
    )
    setattr(system, "data", system.data.tolist())
    setattr(system, "err", system.err.tolist())

    dict = vars(system)

    # Convert numpy arrays to lists
    dict1 = {}
    for k, v in dict.items():
        if isinstance(v, np.ndarray):
            v0 = v.tolist()
        else:
            v0 = v
        if k == "data_tau0" or k == "err_tau0":
            continue
        dict1[k] = v0


    with open(write_file, 'w') as f:
        json.dump(dict1, f, indent=4)


def combine_Ps2_varM_T3000(estimator):
    # Concatenate two calculations into one for json generation.
    # The second calculation adds nuclear masses m0.00108 and m0.00217
    system1 = reading.load_system("datas/Ps2_varM_T3000/"+estimator+"_data.json")
    #system1.add_tau0_extrapolation()

    system2 = reading.load_system("datas/Ps2_varM_T3000_new_datapoints/"+estimator+"_data.json")
    #system2.add_tau0_extrapolation()
    assert(len(system2.range2) == 4) ## two data points of masses

    combined_masses =  np.hstack((system2.range2, system1.range2))
    combined_data = np.hstack((system2.data, system1.data))
    combined_err = np.hstack((system2.err, system1.err))

    system3 = system1  # I know, this is not a copy,
    system3.range2 = combined_masses
    system3.data = combined_data
    system3.err = combined_err

    return system3

