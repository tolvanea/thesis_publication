"""
Collection of random tools in plotting that may even be resycled,
maybe, with modifications at least.
"""

import pickle
import numpy as np                      # pip3 install numpy
import matplotlib.pyplot as plt         # pip3 install matplotlib
from matplotlib.colors import to_rgb
#import uncertainties                    # pip3 install uncertainties

from matplotlib import markers
from matplotlib.path import Path

from tools import extrapolation


def set_errobar_fake_transparency(line, caps, barlines, alpha=0.3):
    # This function exists, because pdf-import of thefigures does not support
    # color transparency. This sets plt.errorbar()'s color lighter so that
    # alpha=0 is the original color and alpha=1 is white. Also works for normal
    # plt.plot() objects, if you set caps and barlines to 'None'
    def brighten(color, amount):
        """amount == 0.0: no change, amount == 1.0: white """
        return tuple(map(lambda c: c + (1-c)*amount, color))
    color = to_rgb(line.get_color())
    new_color = brighten(color, alpha)
    if caps is not None:
        for cap in caps:
            cap.set_color(new_color)
    if barlines is not None:
        for barline in barlines:
            barline.set_color(new_color)
    if line.get_marker() is not None:
        line.set_markerfacecolor(new_color)
        line.set_markeredgecolor(new_color)

def hide_error_bar_with_fake_transparency(caps, barlines):
    for cap in caps:
        cap.set_color((1.0, 1.0, 1.0))
    for barline in barlines:
        barline.set_color((1.0, 1.0, 1.0))

# https://stackoverflow.com/a/26726237
def align_marker(marker, halign='center', valign='middle',):
    """
    create markers with specified alignment.

    Parameters
    ----------

    marker : a valid marker specification. For example ">" or "o"
      See mpl.markers

    halign : string, float {'left', 'center', 'right'}
      Specifies the horizontal alignment of the marker. *float* values
      corrsepons (0 is 'center', -1 is 'right', 1 is 'left').

    valign : string, float {'top', 'middle', 'bottom'}
      Specifies the vertical alignment of the marker. *float* values
      corrsepons (0 is 'middle', -1 is 'top', 1 is 'bottom').

    Returns
    -------

    marker_array : numpy.ndarray
      A Nx2 array that specifies the marker path relative to the
      plot target point at (0, 0).

    Notes
    -----
    The mark_array can be passed directly to ax.plot and ax.scatter, e.g.::

        ax.plot(1, 1, marker=align_marker('>', 'left'))

    """

    if isinstance(halign, (str)):
        halign = {'right': -1.,
                  'middle': 0.,
                  'center': 0.,
                  'left': 1.,
                  }[halign]

    if isinstance(valign, (str)):
        valign = {'top': -1.,
                  'middle': 0.,
                  'center': 0.,
                  'bottom': 1.,
                  }[valign]

    bm = markers.MarkerStyle(marker) # Define the base marker

    # Get the marker path and apply the marker transform to get the
    # actual marker vertices (they should all be in a unit-square
    # centered at (0, 0))
    m_arr = bm.get_path().transformed(bm.get_transform()).vertices

    # Shift the marker vertices for the specified alignment.
    m_arr[:, 0] += halign / 2
    m_arr[:, 1] += valign / 2

    return Path(m_arr, bm.get_path().codes)


# Matplotlib bug draws symbols slightly off-center by default. My autism suffers
# These commented markers fixes png exports. But those that are below
# uncommented, fixes pdf:s. In plt.plot() arguments, unpack like "**dot"
#dot  = {"marker": align_marker(".", halign=-0.10),                 "markersize": 4}
#dash = {"marker": align_marker("$-$", valign=-0.37, halign=-0.24), "markersize": 8}
#xxx  = {"marker": align_marker("x", valign=0.15, halign=-0.15),    "markersize": 5}
#XXX  = {"marker": align_marker("x", valign=0.02, halign=-0.15),    "markersize": 8}
#ooo  = {"marker": align_marker("o", valign=0.0, halign=-0.15),     "markersize": 6}
dot  = {"marker": align_marker("."),    "markersize": 4}
dash = {"marker": align_marker("$-$", valign=-0.37, halign=-0.15),  "markersize": 8}
xxx  = {"marker": align_marker("x"),    "markersize": 5}
XXX  = {"marker": align_marker("x"),    "markersize": 8}
ooo  = {"marker": align_marker("o"),    "markersize": 6}
ref1 = {"marker": align_marker("$\\rightarrow$", halign=-1.45),     "markersize": 22}
ref2 = {"marker": align_marker(">", halign=-2.0),                  "markersize": 12}


# Create plot axes that is ripped in two y-parts with sharing x-axis
def split_axes(xlim, ylim1, ylim2, fig_name, ax_ratio=1/2):
    fig = plt.figure(figsize=(4.0, 3.5), num=fig_name) # larger, used in diploma thesis
    ax2 = plt.subplot2grid((3, 1), (0, 0), colspan=1, rowspan=1)
    ax1 = plt.subplot2grid((3, 1), (1, 0), colspan=1, rowspan=2)

    sep = 0.13
    fig.subplots_adjust(hspace=sep)  # adjust space between axes
    ax2.set_ylim(ylim2)
    ax1.set_ylim(ylim1)
    ax2.set_xlim(xlim)
    ax1.set_xlim(xlim)

    ax2.spines['bottom'].set_visible(False)
    ax1.spines['top'].set_visible(False)
    ax2.xaxis.tick_top()
    ax2.tick_params(axis='x', which='both',length=0)
    ax2.tick_params(labeltop=False)  # don't put tick labels at the top
    ax1.get_yaxis().get_offset_text().set_visible(False)
    ax1.xaxis.tick_bottom()

    # Rip line
    M = 21
    rip_length = 0.08
    rip_x = np.linspace(-1/42, 1 + 1/(2*(M-1)), M)
    rip_y = np.ones(len(rip_x)) - rip_length/2 * (0.5/ax_ratio)
    rip_y[np.arange(M)%2==1] += rip_length* (0.5/ax_ratio)
    for py in [0.0, sep * 0.3 / ax_ratio]:
        ax1.plot(rip_x, rip_y+py, transform=ax1.transAxes, color='k', lw=0.7, mec='k', mew=1, clip_on=False)
        #for px in [0,1]:
            #ax1.plot(rip_border_x+px, rip_border_y+py, transform=ax1.transAxes, color='k', lw=2, mec='k', mew=1, clip_on=False)

    ax2.yaxis.set_label_coords(-0.1,1.02)

    return fig, ax1, ax2

def create_figure_axis(name):
    """
    WARNING! This changes global font of all plots after calling this function!
    """
    # Use latex font in labels and legends to match document font
    # I found out that revtex4-2 uses Latin Modern Roman with
    # latex command: \expandafter\show\the\font
    # With it, I got for : "[lmroman10-regular]:+tlig"
    plt.rcParams['text.usetex'] = True
    plt.rcParams['text.latex.preamble'] = r'\usepackage{lmodern}'
    plt.rcParams["font.family"] = "Latin Modern Roman"
    plt.rcParams.update({'font.size': 12})

    fig, ax = plt.subplots(1, 1, figsize=(4.0, 3.5), num=name)
    ax.set_xlabel("$T$ $(\\mathrm{{K}})$")
    ax.set_ylabel("$-\\chi \\; (10^{-11} \\frac{\\mathrm{m}^3}{\\mathrm{mol}})$", rotation='horizontal')
    ax.yaxis.set_label_coords(-0.03,1.03)
    fig.subplots_adjust(bottom=0.15, left=0.16)
    return ax, fig

#
def plot_references(ax, refdata):
    """
    Plot reference points from list of rows.

    one row of list 'refdata' is either
        x, y, (label, color)
    or
        x, y, (label, color, marker)
    or
        x, y, (label, color, marker, markersize)
    or
        x, y, index
    where
        - x, and y are coordinates of reference point
        - (label, color, marker, size) will be passed to matplolib errorbar plottgng
        - index points to earlier row, so that the rows values (label, color, marker, size)
          will be copied to this new point
    If marker is omitted, then a dot is used
    """
    for row in refdata:
        size = None
        #markers = ["o", "v", "^", "s", "D"]
        marker='o'
        if isinstance(row[2], tuple): # New data point have unique marker
            settings = row[2]
            label = settings[0]
        else:  # using index to earlier marker
            index = row[2]
            assert(isinstance(index, int))
            settings = refdata[index][2]
            label = None
        color = settings[1]
        if len(settings) > 2:
            marker = settings[2]
        if len(settings) > 3:
            size = settings[3]
        line, caps, barlines = ax.errorbar(
            row[0],
            row[1],
            [0.0],  # zero length errorbars so that it does not mess the legend
            label=label,
            ls="",
            marker=marker,
            markersize=size,
            color=color
        )
        hide_error_bar_with_fake_transparency(caps, barlines)
