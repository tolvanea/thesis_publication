"""
Collection of random tools in data-analysis that may even be resycled,
maybe, with modifications at least.

Contains fitting methods
    - 'linear'              y = a*x + b
    - 'power_coeff'         y = a*x^b + c
    - 'exponential'         y = a*e^(b*x) + c
    - 'inverse_polynomial'  y = a/x^3 + b/x^2 + c/x + d
"""
import numpy as np
import uncertainties
from uncertainties import unumpy as unp
import scipy.optimize

#######################################################
# Public functions of this file: Extrapolate point or vector by giving x, y and error -data.
# Errorbar is extrapolated also


def linear_errorbardata_fit(x_arr, mean_arr, err_arr, x0, analytical_method=True):
    """
    Linear fit a*x + b estimate and errorbars (i.e. confidence intervals) at x0
    Returns 1-sigma errorbars.

    Extrapolates (or interpolates) data with uncertainties, and estimates also
    uncertainty at the extrapolation. Calculates 1-sigma certainty

    Uses analytic formula
    https://en.wikipedia.org/wiki/Simple_linear_regression#Normality_assumption


    """

    if analytical_method:  # Use new (mathematically proper) method
        """
        This is a mathematically exact way to do it. Uses (joint) student's
        t distribution of a and b, which results hyperbolic confidence intervals
        curves.
        """

        # For reference, here are common p-values:
        #   1*sigma -> p_value = 1-0.682689
        #   2*sigma -> p_value = 1-0.954499
        #   3*sigma -> p_value = 1-0.997300
        try:
            a, b, _, _, f = line_fit_confidense_intervals(
                x_arr, mean_arr, err_arr, p_value=1-0.682689
            )
        except ValueError:  # Too many nans in data to fit a line
            if np.isscalar(x0):
                return np.nan, np.nan
            else:
                nan_vec = np.full(len(x0), fill_value=np.nan)
                return nan_vec, nan_vec

        return a*x0 + b, f(x0)
    else:  # Use same method that is used for extrapolation of other functions
        """
        See input parameters from jack_knive_estimate()

        This is not mathematically exact, but it is somewhat a hack using jack
        knife sampling method.

        This method gives about correct errorbars for 1*sigma, but sadly
        not for 2*sigma. Analytical gives ~1.7× larger errorbars for 2sigma.
        The reason for this is that student's t distribution has long tail,
        and 2*sigma certainty requires 3.4 * 1sigma error deviation.
        """
        def linear_estim(x_arr, y_sample, x0):
            # Fit line to a single sample
            model = teach_linear(x_arr, y_sample)
            return predict_linear(x0, model)

        mean, err = jack_knive_estimate(x_arr, mean_arr, err_arr, x0, linear_estim)
        return mean, err

def power_coeff_errorbardata_fit(x_arr, mean_arr, err_arr, x0, c_lower_bound, c_upper_bound, mc_lines=400):
    """
    Fit power coefficient a*x^b + c

    Extrapolates (or interpolates) data with uncertainties, and estimates uncertainty
    at the extrapolation.

    Parameters 'c_lower_bound' and 'c_upper_bound' determine the y-range at which offset 'c' is
    searched.  This 'c' is y-value at which exponential curve vanishes to (+ or -) infinity at
    x-axis. That is, make sure that bounds include the y-point at which curve goes to infinity.

    See input parameters from jack_knive_estimate()
    """

    def power_estim(x_arr, y_sample, x0):
        model = teach_power_coefficient(
            x_arr, y_sample, c_lower_bound, c_upper_bound, c_accuracy=6, resolution=30
        )
        return predict_power_exponent(x0, model)

    mean, err = jack_knive_estimate(x_arr, mean_arr, err_arr, x0, power_estim, mc_lines)
    return mean, err


def exponential_errorbardata_fit(x_arr, mean_arr, err_arr, x0, c_lower_bound, c_upper_bound, mc_lines=400):
    """
    Fit exponential a*e^(b*x) + c

    Extrapolates (or interpolates) data with uncertainties, and estimates uncertainty
    at the extrapolation.

    Parameters 'c_lower_bound' and 'c_upper_bound' determine the y-range at which offset 'c' is
    searched.  This 'c' is y-value at which exponential curve vanishes to (+ or -) infinity at
    x-axis. That is, make sure that bounds include the y-point at which curve goes to infinity.

    See input parameters from jack_knive_estimate()
    """

    def exp_estim(x_arr, y_sample, x0):
        model = teach_exponential(
            x_arr, y_sample, c_lower_bound, c_upper_bound, c_accuracy=6, resolution=30
        )
        return predict_exponential(x0, model)
    mean, err = jack_knive_estimate(x_arr, mean_arr, err_arr, x0, exp_estim, err_sigma, mc_lines)
    return mean, err

def inverse_polynomial_errorbardata_fit(
    x_arr, mean_arr, err_arr, x0,
    mc_lines=400, constant_term=True, log_y=False, powers=None
):
    """
    Fits inverse polynomial
        y = a/x^3 + b/x^2 + c/x + d
    or if powers=[e,f,...g] are given, fit
    y = a/x^e + b/x^f + ... c/x^g + d
    Powers can be positive rational numbers.

    NOTE! All coefficients a-d will have the same sign!


    Extrapolates (or interpolates) data with uncertainties, and estimates also uncertainty
    at the extrapolation.

    'constant_term = True' means that 'd' is found also

    'log_y = True' means that :
        - inverse root fit is used rather than power fit. Inverse roots are straight lines
          in log-log plot
        - fit residual weighting is optimized so that it looks good in log-y plot. However,
          this is practically unnoticeable.

    See input parameters from jack_knive_estimate()
    """

    def invpol_estim(x_arr, y_sample, x0):
        model = teach_inverse_polynomial(x_arr, y_sample, constant_term, log_y, powers)
        return predict_inverse_polynomial(x0, model, constant_term, powers)

    mean, err = jack_knive_estimate(x_arr, mean_arr, err_arr, x0, invpol_estim, mc_lines)
    return mean, err



def proper_mean_with_errorbars(values, errors):
    """
    In a sense this fits a function y = c, where c is a constant

    Given multiple values (or arrays) with uncertainty, this calculates
    the mean for them. Note, the mean is NOT sum/N, because the sum is
    weighted by variances.
    https://en.wikipedia.org/wiki/Weighted_arithmetic_mean
    https://physics.stackexchange.com/a/329412
    and insert there w_i = 1/sigma_i^2

    values  List of values (or list of arrays) that will be averaged
    errors  List of errorbars (sigma-1) of 'values'.
            With N samples, this error is std_dev/sqrt(N)
    """
    weighted_sum = values[0]*0
    total_weight = values[0]*0
    for v, e in zip(values, errors):
        weight = 1/e**2
        weighted_sum += v * weight
        total_weight += weight
    weighted_average = weighted_sum / total_weight
    error_of_weighted_average = 1 / np.sqrt(total_weight)
    return weighted_average, error_of_weighted_average


#######################################################
# Also public functions for this file, for purpose if no errorbar fitting is needed
# Functions 'teach' and 'predict' for above functions.
# - 'teach' takes in x and y data, and outputs a model.
# - 'tredict' takes in extrapolation x-values and the model



# Given x and y-data, this finds coefficients a and b to fit line
# ax + b
def teach_linear(x, y):
    return np.polyfit(x, y, 1)

# Given coefficients a and b, and x-values this calculates
# y = ax + b
def predict_linear(x0, model):
    a, b = model
    return a * x0 + b

# Power exponent curve fit. Given x and y-data, it finds coefficients a, b, c so that
# y = a*x^b + c
# You must give finite bounds for c, so that c is solved iteratively from them.
# For other arguments, see 'teach_nonlinear_function()'
def teach_power_coefficient(xdata, ydata, c_lower_bound, c_upper_bound, c_accuracy = 6, resolution=50):
    """
    Math derivation for power exponent curve in logarithmic coodrinates
    y = a*x^b + c
      = a*e^(logx*b) + c        | log()
    log(y-c) = loga + logx * b
    which is a linear problem with respect to log variables
    """

    # Solves coefficients a,b from: 'y-c = a*x^b' with linear regression.
    # Return None if fitting failed
    def least_squares_solve(c, x, y):
        if np.isinf(c):
            return None
        # Assume the sign of y-data from the largest element
        sign = np.sign(y[np.argmax(np.abs(y-c))]-c)
        y_c = sign*(y-c)  # This is now hopefully somewhat mostly positive data
        positive = y_c > 0

        if np.sum(positive) < 2: # 2 dimensions for a and b
            return None

        # Solve linear problem derived above
        # Weight parameter "w" is normalized so that residuals of logarithmic y-values
        # are balanced between large and small values.
        # https://stackoverflow.com/questions/3433486/how-to-do-exponential-and-logarithmic-curve-fitting-in-python-i-found-only-poly
        # Let Y_i = log(y_i), and let D (\Delta) notate the redidual difference
        #   DY_i = Y_i - Y_hat
        #        = log(y_i) - log(y_hat)               | Dy_i = y_i - y
        #        ->
        #    - DY_i = - log(y_i) + log(y_i - Dy_i)     | y_i > 0
        #        = log((y_i - Dy_i) / y_i)
        #        = log(1- Dy_i/y_i)                    | Dy_i/y_i << 1
        #        = - Dy_i/y_i
        #        ->
        #    Dy_i = DY_i * y_i                         | y_i > 0
        # Therefore weight is 'w=np.sqrt(y_c[positive])'. The square root part comes from that the
        # weight is somehow applied to residual DY_i^2 and not to the DY_i.
        info = np.polyfit(
            np.log(x[positive]),
            np.log(y_c[positive]),
            deg=1,
            w=np.sqrt(y_c[positive]),
            full=True
        )
        # full info is (coeff, residuals, rank, singular_values, rcond)
        coeff = info[0]
        rank = info[2]
        if rank < 2:
            return None

        b, loga = coeff
        a = sign*np.exp(loga)
        return (a, b)

    # flip_y flips exponential curve betwee decreasing <-> increasing
    def minimization_function(c):
        solution = least_squares_solve(c, xdata, ydata)
        if solution is None:
            return np.inf
        else:
            a, b = solution
            y_fit = a * xdata**b + c
            return np.sum((ydata-y_fit)**2)  # Minimize least square error

    return teach_nonlinear_function(
        xdata, ydata, c_lower_bound, c_upper_bound, minimization_function,
        least_squares_solve, c_accuracy, resolution
    )

# Given coefficients a, b, c, and x-values this calculates
# power exponent curve y = a*x^b + c
# y = a*e^(b*ln(x))
def predict_power_exponent(x0, model):
    a, b, c =  model
    y = a*x0**b + c
    return y


# Exponential curve fit. Given x and y-data, it finds  coefficients a, b, c so that
# y = a*e^(b*x) + c
# You must give finite bounds for c, so that c is solved iteratively from them.
# For other arguments, see 'teach_nonlinear_function()'
def teach_exponential(xdata, ydata, c_lower_bound, c_upper_bound, c_accuracy = 6, resolution=50):
    """
    Math derivation: Exponential curve in logarithmic coodrinates
    y = a*e^(b*x) + c        | log()
    log(y-c) = loga + x * b
    which is a linear problem with respect to log variables
    """

    # Solves coefficients a,b from: 'y-c = a*e^(b*x)' with linear regression.
    # Return None if fitting failed
    def least_squares_solve(c, x, y):
        if np.isinf(c):
            return None
        # Assume the sign of y-data from the largest element
        sign = np.sign(y[np.argmax(np.abs(y-c))]-c)
        y_c = sign*(y-c)  # This is now hopefully somewhat mostly positive data
        positive = y_c > 0

        if np.sum(positive) < 2: # 2 dimensions for a and b
            return None

        # Solve linear problem derived above
        # See weighting w documentation in 'teach_power_coefficient'
        info = np.polyfit(
            x[positive],
            np.log(y_c[positive]),
            deg=1,
            w=np.sqrt(y_c[positive]),
            full=True
        )
        # full info is (coeff, residuals, rank, singular_values, rcond)
        coeff = info[0]
        rank = info[2]
        if rank < 2:
            return None

        b, loga = coeff
        a = sign*np.exp(loga)

        return (a, b)

    # flip_y flips exponential curve betwee decreasing <-> increasing
    def minimization_function(c):
        solution = least_squares_solve(c, xdata, ydata)
        if solution is None:
            return np.inf
        else:
            a, b = solution
            y_fit = a * np.exp(b*xdata) + c
            return np.sum((ydata-y_fit)**2)  # Minimize least square error

    return teach_nonlinear_function(
        xdata, ydata, c_lower_bound, c_upper_bound, minimization_function,
        least_squares_solve, c_accuracy, resolution
    )

# Given coefficients a, b, c, and x-values this calculates
# power exponent curve y = a*x^b + c
# y = a*e^(b*ln(x))
def predict_exponential(x0, model):
    a, b, c =  model
    y = a*np.exp(b*x0) + c
    return y


"""
Given x and y-data, this finds coefficients a, b, c to fit
inverse polynomial
    y = a/x^3 + b/x^2 + c/x + d
or if powers=[e,f,...g] are given, fit
    y = a/x^e + b/x^f + ... c/x^g + d
# Powers can be positive rational numbers.

NOTE! All coefficients a-d will have the same sign!

'constant_term = True' means that 'd' is found also
'log_y = True' means that fit is optimized so that it looks good in log-y plot.
"""
def teach_inverse_polynomial(x, y, constant_term=True, log_y=False, powers=None):
    mat = inverse_polynom_mat(x, constant_term, powers)

    # We want coefficients a,b,c,d to have  all same signs, so we use
    # 'scipy.optimize.nnls' instead of 'np.linalg.lstsq'.
    # The 'np.linalg.lstsq' solves x in linear equation Ax=y,
    # and 'scipy.optimize.nnls' solves it for positive y.

    if not log_y:
        model1, residual1 = scipy.optimize.nnls(mat, y)
        model2, residual2 = scipy.optimize.nnls(mat, -y)
    else:
        # The idea here is to apply weights to least squares residuals so that a fitting to
        # log-y graph looks better. However, any set of weights (> 0) produces practically
        # identical looking fits with each other. So this is more a theoretical improvement.
        # The weights are chosen here to be (nearly) optimal to log-y graph using residual
        # derivation in documentation of function 'teach_power_coefficient'.
        # Let Y_i = log(y_i), and let D (\Delta) notate the redidual difference
        # The derived weigth scaling is: DY_i = Dy_i / y_i = Dy_i * (1 / exp(Y_i))
        y_normed = np.abs(y) / np.max(np.abs(y))  # Normed for floating point accuracy
        W = 1 / y_normed  # weights
        mat_w = mat * np.sqrt(W[:,np.newaxis])
        y_w = y * np.sqrt(W)

        model1, residual1 = scipy.optimize.nnls(mat_w, y_w)
        model2, residual2 = scipy.optimize.nnls(mat_w, -y_w)

    if residual1 < residual2:
        model = model1
    else:
        model = -model2

    return model

# Given coefficients a, b, c, and x-values this calculates
# inverse polynomial
#    y = a/x^3 + b/x^2 + c/x + d
# or if log_y==True then fit
#    y = a/x^(1/3) + b/x^(1/2) + c/x + d
def predict_inverse_polynomial(x0, model, constant_term=True, powers=None):
    y = inverse_polynom_mat(x0, constant_term, powers) @ model
    return y




#######################################################
# More private-y functions, that are not part of public interface

def line_fit_confidense_intervals(x_array, y_array, err_array, p_value=0.05):
    # Takes in data with errorbars, and outputs 'a_err' and 'b_err' for
    # equation 'y = ax + b'. That is, confidence interval of
    # 'a' and 'b'. Also, this function returns error function 'f' for
    # estimated linear fit so that 'y = ax + b +/- f(x)'
    # Internal mechanism of this function is that it generates samples
    # from errorbars, and then applies an equation taken from wikipedia.

    # For reference, here are common p-values:
    #   1*sigma -> p_value = 1-0.682689
    #   2*sigma -> p_value = 1-0.954499
    #   3*sigma -> p_value = 1-0.997300

    out = drop_out_nans_and_infs(x_array, y_array, err_array)
    if out is not None:
        x, y, err = out
    else:
        raise ValueError("Insufficient amount of not-crap data, that isn't NaN or inf. ")


    def errorbars_to_discrete_samples(x, y, p_value=0.05):
        # Confidence intervals on 'a' and 'b' in linear fit 'y = ax + b'
        # This function based on:
        # https://en.wikipedia.org/wiki/Simple_linear_regression#Normality_assumption
        # Note that commonly used symbols are "y = beta*x + alpha".
        a, b = np.polyfit(x, y, 1)

        residual = np.sum((a*x+b - y)**2)
        x_var_unnorm = np.sum((x - x.mean())**2)
        n = len(x)
        sem_slope = np.sqrt((1/(n-2) * residual) / x_var_unnorm)
        sem_constant = sem_slope * np.sqrt(1/n * np.sum(x**2))

        p = (1-p_value/2)
        # Student's t quantile for dof=2
        # https://en.wikipedia.org/wiki/Quantile_function#Student's_t-distribution
        t_quantile_2df = 2*(p-0.5)*np.sqrt(2/(4*p*(1-p)))

        # The same can be calculated with scipy. Uncomment to test it:
        # from scipy.stats import t
        # quantile_2df_scipy = t.ppf(p, df=2)
        # assert(np.isclose(t_quantile_2df, quantile_2df_scipy))

        # Alternative way to derive result for 1sigma (p_value=(1-0.68)):
        # Standard deviation of 'a' and 'b'. I derived this, and verified by:
        # https://online.stat.psu.edu/stat415/lesson/7/7.5
        # This gives 'a_err' within 20% but 'b_err' is only 1/3 from above.
        #b_err = np.sqrt((1/n * residual) / n)
        #a_err = np.sqrt((1/n * residual) / x_var_unnorm)
        #error_hyperbola = lambda x_in: np.sqrt(
            #a_err**2 * (x_in-x.mean())**2
            #+ b_err**2 * x_var_unnorm/np.sum(x**2)
        #)

        a_err = sem_slope * t_quantile_2df
        b_err = sem_constant * t_quantile_2df
        error_hyperbola = lambda x_in: np.sqrt(
            1/(n-2) * residual
            * (1/n + (x_in-x.mean())**2 / x_var_unnorm)
        ) * t_quantile_2df
        return a, b, a_err, b_err, error_hyperbola


    N = 1000
    err_sigma = 1
    generated_samples_x = np.zeros(len(x) * N)
    generated_samples_y = np.zeros(len(x) * N)
    for i in range(len(x)):
        I = i*N
        generated_samples_x[I:I+N] = x[i]
        rnd = np.random.randn(N)
        rnd -= rnd.mean()
        generated_samples_y[I:I+N] = y[i] + rnd * (err[i]/err_sigma)*np.sqrt(N)

    return errorbars_to_discrete_samples(
        generated_samples_x, generated_samples_y, p_value=p_value
    )


def drop_out_nans_and_infs(x_array, mean_array, err_array):
    # Drop out NaN/infinite values from mean_array and err_array
    # On succes, return mean_arr, err_arr
    # On error,  return None
    finite = np.logical_and(np.isfinite(mean_array), np.isfinite(err_array))
    x_arr = x_array[finite]
    mean_arr = mean_array[finite]
    err_arr = err_array[finite]
    if finite.sum() < 2:
        return None
    return x_arr, mean_arr, err_arr

def jack_knive_estimate(x_array, mean_array, err_array, x0, fun, err_sigma=1, mc_lines=400, cut_off_sigma=3):
    """
    Interpolates or extrapolates data with errorbars, and estimates also variance at that point.

    Error estimates are implemented with Monte Carlo sampling and jack-knife unbiasing. See
        https://young.physics.ucsc.edu/jackboot.pdf
    for more about jack-knive method.

    To demonstrate the problem, let's imagine a simplified case we have data vector (of scalars)
    [d0...d_M] and a scalar function f(d). We are calculating f(<d_i>), where <> is mean and want to
    find out the variance of it. In nutshell, the problem is here that we can not estimate variance
    of function as var(f(d_i)) = <(f(d_i) - f(<d_i>))^2>, because that would result variance
    <f(_di)>, not f(<d_i>). This is what jack knife method solves, and it is the magic is here.
    Read that beginning of section III from that paper to understand what is made here.

    In a sense what we make is weird: First we use standard deviations of data to generate samples,
    and right after we average it with jack knife. There may be some analytical methods to calculate
    the same thing without redundant computation of sample-generation, but this method works too.
    # Parameters:
    x_array:        x-values                                (Iterable[Float])
    mean_array:     y-means, also referred below as "data"  (Iterable[Float])
    err_array:      y-errorbars                             (Iterable[Float])
    x0:             x-value(s) in which to interpolate or extrapolate  (Float or 1d-array)
    fun:            function cabable of estimating x0 with inputs as fun(x_array, mean_array, x0)
    err_sigma:      Standard deviation sigma in errorbars: 1 -> 68%,  2 -> 95%,  3 ->99.7% ...
                        This sigma must correspond sigma of `err_array`. Result errors are also
                        deviated by this same sigma.
    mc_points:      Errorbar distribution is scattered this many individual Monte Carlo points
    mc_lines:       This many linear fits are (Monte Carlo) sampled from random set of points
    cut_off_sigma:  Drop out badly beaving curve-fits by considering only those within
                    of n*sigma from mean, where "sigma" is standard deviation.
    return:            (y_mean, y_err) which are floats or 1d-arrays, depending on x0
    """
    assert((len(x_array) == len(mean_array)) and (len(err_array) == len(mean_array)))

    out = drop_out_nans_and_infs(x_array, mean_array, err_array)
    if out is not None:
        x_arr, mean_arr, err_arr = out
    else:
        if np.isscalar(x0):
            return np.nan, np.nan
        else:
            nan_vec = np.full(len(x0), fill_value=np.nan)
            return nan_vec, nan_vec

    fit_mean = fun(x_arr, mean_arr, x0)

    # Unbiased error estimate of fit with jack-knife method
    # So, let's notate line fit "f(data)", where 'data' is input vector. We are interested of
    # error on <f(data_k)> where data_k is jack-knife average sample k. The jack-knife mean is taken
    # from randomly generated gaussian data, that is generated from errobar data.
    jackknife_data_averages = np.zeros((len(mean_arr), mc_lines))
    for i in range(len(mean_arr)):
        randn = np.random.randn(mc_lines)
        randn -= randn.mean()  # Remove bias from generated samples, as we sample only variance here
        # Multiplied by sqrt(mc_lines) because err_arr is std of sample mean, not std of
        # sample. Increasing mc_lines does not increase errorbars, because the following line
        # divides with it.
        data_sample = mean_arr[i] + randn * (err_arr[i]/err_sigma) * np.sqrt(mc_lines)
        jackknife_data_averages[i,:] = (data_sample.sum() - data_sample) / (mc_lines-1)

    if np.isscalar(x0):
        jackknife_fit_estimates = np.zeros(mc_lines)
        for k in range(mc_lines):
            jackknife_fit_estimates[k] = fun(x_arr, jackknife_data_averages[:,k], x0)
        _, cropped = crop_off_bad_values(jackknife_fit_estimates, cut_off_sigma=cut_off_sigma)
        error_estimate = cropped.std() * np.sqrt(mc_lines - 1) * err_sigma
    else:
        jackknife_fit_estimates = np.zeros((mc_lines,len(x0)))
        for k in range(mc_lines):
            jackknife_fit_estimates[k,:] = fun(x_arr, jackknife_data_averages[:,k], x0)

        # remove bad fits, mainly needed by unlinear fits wich may blow up with bad set of samples
        valid = np.ones(mc_lines, dtype=np.bool_)
        for i in range(len(x0)):
            v, _, = crop_off_bad_values(jackknife_fit_estimates[:,i], cut_off_sigma=cut_off_sigma)
            valid = np.logical_and(valid, v)
        cropped = jackknife_fit_estimates[valid,:]

        error_estimate = np.zeros(len(x0))
        for i in range(len(x0)):
            error_estimate[i] = cropped[:,i].std() * np.sqrt(mc_lines - 1) * err_sigma
    return fit_mean, error_estimate

def crop_off_bad_values(data, cut_off_sigma=5):
    """
    Crop off values that are very far away from other points. Namely, if point is more
    than 5 standard deviations¹ away from other data points, it will be dropped. Due
    to the probabilistic nature, one in 1.7 million will be falsely dropped out.
    Also drop out nans and infinities.

    ¹ Actually, strandard deviation is calculated from 64% of data, not taking
    highest and lowest 18% of data points into account. Then the resulting std
    of cropped set is multiplied with two to result approximate deviation.

    :param data:
    :return:
        valid   bool array with length of data, in which False means that data
                is dropped out
        data    valid data
    """
    finite = np.isfinite(data)
    data1 = data[finite]

    # Drop highest 20% and lowest 20% of data away for calculation of standard deviation
    assert(len(data1.shape)==1) # 1d data only
    ids_of_sorted = np.argsort(data1)
    l = int(len(data1) * 0.18)
    data_ends_cropped = data1[np.sort(ids_of_sorted[l:-l])]

    if len(data_ends_cropped) < 2:
        return np.zeros(len(data)), np.array([])

    mean = data_ends_cropped.mean()
    err = data_ends_cropped.std() * 2  # Multiplied with 2 to counteract the 2*18% loss
    datacopy = data.copy()
    datacopy[~finite] = np.inf
    valid = np.abs(datacopy - mean) <= (err * cut_off_sigma)
    return valid, data[valid]


# Teach curve fit by minimizing a single-parameter function, which takes in
# a parameter 'c'.
# You must give finite bounds for c, so that c is solved iteratively from them.
# (It doesn't matter if lower_bound > upper_bound.)
# Minimization is a for-loop, with iterative looping nearer and nearer the minimum.
# Arguments:
#   - c_accuracy=1, resolution=50 finds c with 2% error to bounds, and
#     c_accuracy=6, resolution=50 finds c with 2%^6 error but is 6x slower.
#   - The resolution is inverse of the presentage above. For example, 2% = 1/50
def teach_nonlinear_function(xdata, ydata, c_lower_bound, c_upper_bound, minimization_function, least_squares_solve, c_accuracy=6, resolution=50):
    # Find best value for fixed c in 'y = a*x^b + c'
    c_min = None
    err_min = np.inf
    assert(c_lower_bound*c_upper_bound > 0.0)  # Limits must be the same sign!
    sign = np.sign(c_lower_bound)  # Flip to positive if negative
    low = min(sign*c_lower_bound, sign*c_upper_bound)
    high = max(sign*c_lower_bound, sign*c_upper_bound)
    for _ in range(c_accuracy):
        geomspace_delta = (np.log(high)-np.log(low))/(resolution-1)
        for c in np.geomspace(low, high, resolution):
            square_error = minimization_function(sign*c)
            if square_error < err_min:
                c_min = c
                err_min = square_error
                low_min = np.exp(np.log(c) - geomspace_delta)
                high_min = np.exp(np.log(c) + geomspace_delta)
        low, high = low_min, high_min
    c_min = sign*c_min

    result = least_squares_solve(c_min, xdata, ydata)
    assert(result is not None)
    a, b = result
    model = a, b, c_min
    return model


# Construct inverse polynomial matrix for least squares fit. By default it is
#    y = a/x^3 + b/x^2 + c/x + d
# or if powers=[e,f,...g] are given, fit
#   y = a/x^e + b/x^f + ... c/x^g + d
# Powers can be positive rational numbers.
# Giving constant_term=False drops the term 'd'
def inverse_polynom_mat(x, constant_term=True, powers=None):
    if powers is None:
        m = np.vstack((
            1/x,
            1/x**2,
            1/x**3,
            np.ones(len(x)),
        )).T
    else:
        m = np.vstack([1/x**p for p in powers] + [np.ones(len(x))]).T

    if constant_term:
        return m
    else:
        return m[:, :-1]
