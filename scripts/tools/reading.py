import json

import numpy as np

from tools import extrapolation

class System:
    def __init__(self, dictionary):
        for k, v in dictionary.items():
            # Convert lists to numpy arrays
            if isinstance(v, list):
                v0 = np.array(v)
                if isinstance(v0.flat[0], float):
                    v0 = v0.astype(np.float64)
            else:
                v0 = v
            setattr(self, k, v0)

    def add_tau0_extrapolation(self):
        """
        Adds attributes 'data_tau0' and 'err_tau0' which are vectors at tau=0 extrapolation
        """
        assert(self.latex_symbol1 == "\\tau")  # first dimension of data matrix is not tau!
        y_ext0 = []
        err_ext0 = []
        for i in range(self.data.shape[1]):
            y_at_0, err_at_0 = extrapolation.linear_errorbardata_fit(
                self.range1, self.data[:,i], self.err[:,i], 0.0
            )
            y_ext0.append(y_at_0)
            err_ext0.append(err_at_0)

        self.data_tau0 = np.array(y_ext0).astype(np.float64)
        self.err_tau0 = np.array(err_ext0).astype(np.float64)
        self.range1_tau0 = np.append(0.0, self.range1)

    def add_tau0_T0_extrapolation(self, ext_fun=None):
        """
        Adds attributes 'data_tau0_T0' and 'err_tau0_T0' which are extrapolated vectors at tau=0
        with varying T, but also including extrapolation at T=0.
        Adds attribute 'range1_T0'

        If extrapolation function 'ext_fun(x, y, err)' is given, then that is used.
        """
        assert(self.latex_symbol2 == "T")  # second dimension of data matrix is not temperature!
        # tau = 0 extrapolation must be calculated first
        if not hasattr(self, "data_tau0"):
            add_tau0_extrapolation()

        if ext_fun is None:
            y_at_T0_tau0, err_at_T0_tau0 = extrapolation.linear_errorbardata_fit(
                self.range2, self.data_tau0, self.err_tau0, 0.0
            )
        else:
            y_at_T0_tau0, err_at_T0_tau0 = ext_fun(
                self.range2, self.data_tau0, self.err_tau0
            )
        self.data_tau0_T0 = np.append(y_at_T0_tau0, self.data_tau0)
        self.err_tau0_T0 = np.append(err_at_T0_tau0, self.err_tau0)
        self.range2_T0 = np.append(0.0, self.range2)


def load_system(write_file):
    with open(write_file) as f:
        data_dict = json.load(f)
    system = System(data_dict)
    return system
