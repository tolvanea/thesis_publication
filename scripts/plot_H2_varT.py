# Plot figure with H2 (AQ and BO) with variating temperature
import numpy as np
import matplotlib.pyplot as plt
import uncertainties as unp

from tools import extrapolation
from tools.extrapolation import linear_errorbardata_fit, proper_mean_with_errorbars
from tools import plotting
from tools.plotting import dot, dash, ref1, ref2
from tools import printing
from tools import reading


def get_H2_AQ_BO_data_at_tau0():
    system_AQ = reading.load_system("datas/H2_varT_AQ/susceptibility_data.json")
    system_BO = reading.load_system("datas/H2_varT_BO/susceptibility_data.json")
    system_AQ.add_tau0_extrapolation()
    system_BO.add_tau0_extrapolation()

    T = system_AQ.range2
    susc_AQ, err_AQ = system_AQ.data_tau0, system_AQ.err_tau0,
    susc_BO, err_BO = system_BO.data_tau0, system_BO.err_tau0
    return T, susc_AQ, err_AQ, susc_BO, err_BO

def get_HD_data_at_tau0():
    system_AQ = reading.load_system("datas/HD_varT/susceptibility_data.json")
    system_AQ.add_tau0_extrapolation()

    T = system_AQ.range2
    susc_AQ, err_AQ = system_AQ.data_tau0, system_AQ.err_tau0,
    # If you want better expponential fitting, use this tiny nudge for that.
    # susc_AQ[1] += 0.02e-11
    return T, susc_AQ, err_AQ

def get_D2_data_at_tau0():
    names = ["Ps2_varM_T300", "Ps2_varM_T1000", "Ps2_varM_T3000"]
    T = np.array([300.0, 1000.0, 3000.0])
    susc = []
    err = []
    for name in names:
        system = reading.load_system("datas/" + name + "/susceptibility_data.json")
        system.add_tau0_extrapolation()
        susc.append(system.data_tau0[-3])
        err.append(system.err_tau0[-3])
    return T, np.array(susc), np.array(err)

def nonlinear_errorbardata_fit(x, y, e, x0, mc_lines=400):
    # This switches between two curve fits
    if True:  # a*x^b + c
        f = extrapolation.power_coeff_errorbardata_fit
    else:     # a*e^(b*x) + c
        f = extrapolation.exponential_errorbardata_fit
    return f(
        x, y, e, x0, mc_lines=mc_lines,
        # Known bounds of T=0 extrapolation
        c_lower_bound=y[0]*0.8,
        c_upper_bound=y[0]*1.2,
    )

def mean_errorbardata_fit(x, y, e, x0):
    mean, err = proper_mean_with_errorbars(y, e)
    return np.ones(len(x0)) * mean, np.ones(len(x0)) * err

def main():
    errorbar_curves = False  # Plot confifence intervals

    print("H2")
    T, susc_AQ, err_AQ, susc_BO, err_BO = get_H2_AQ_BO_data_at_tau0()

    ax, fig = plotting.create_figure_axis("H2")

    plots = [
        ("AQ", T, susc_AQ, err_AQ, "C0", dot,  nonlinear_errorbardata_fit),
        ("BO", T, susc_BO, err_BO, "C1", dash, linear_errorbardata_fit),
        #("AQ linear", T, susc_AQ, err_AQ, "C2", ".", linear_errorbardata_fit),
    ]
    if False:  # Change to True to plot HD and D2
        _, susc_HD, err_HD = get_HD_data_at_tau0()
        _, susc_D2, err_D2 = get_D2_data_at_tau0()
        plots.extend([
            ("HD", T, susc_HD, err_HD, "C2", dot,  linear_errorbardata_fit),
            ("D2", T, susc_D2, err_D2, "C3", dash, linear_errorbardata_fit),
        ])

    for (label, T, chi, err, color, marker, errorbar_fit) in plots:
        print("   ", label)

        # Print bare errorbars of data
        line, caps, bars = ax.errorbar(T, -1e11*chi, 1e11*err, label=label, ls="", c=color, capsize=4, **marker)
        #plotting.set_errobar_fake_transparency(line, caps, bars, alpha=0.1)

        # Fit curve to errorbars
        T_range = np.linspace(0.0, T[-1]*1.1, 41)
        fit, fit_err = errorbar_fit(T, chi, err, T_range)
        print(" "*8, "At T=0:", printing.err_format(fit[0], err[0]))

        # Plot T=0 errorbar with ligher color
        line0, caps0, bars0 = ax.errorbar(0.0, -1e11*fit[0], 1e11*fit_err[0], ls="", c=color, capsize=4, **marker)
        plotting.set_errobar_fake_transparency(line0, caps0, bars0, alpha=0.4)
        if errorbar_curves:
            ax.plot(T_range, -1e11*fit - 1e11*fit_err , ls=":", c=color)
            ax.plot(T_range, -1e11*fit + 1e11*fit_err, ls=":", c=color)
        ax.plot(T_range, -1e11*fit, ls="--", c=color)

    # Plot gray legends
    if errorbar_curves:
        line0, caps0, bars0 = ax.errorbar(
            [np.nan], [np.nan], [0.0], ls="--", c="gray", label="fit"
        )
        plotting.hide_error_bar_with_fake_transparency(caps0, bars0)
        line0, caps0, bars0 = ax.errorbar(
            [np.nan], [np.nan], [0.0], ls=":", c="gray", label="$1\\sigma$ error of the fit"
        )
        plotting.hide_error_bar_with_fake_transparency(caps0, bars0)

    # $\\rightharpoondown$
    refdata_H2 = (
        #    x,          y, (label,color,marker, size)
        (  0.0, 5.1131, ("Reference (AQ)", "C0", *ref1.values())), # 0
        (  0.0, 4.9701, ("Reference (BO)", "C1", *ref2.values())), # 1
        #    x,          y,     index to above
        # AQ, , These copy color of index 0
        (  0.0, 5.0933, 0), # This copies color of index 0
        #(  0.0, 5.2065, 0), # Dropped because under the legend box
        (  0.0, 5.0769, 0),
        (300.0, 5.032,  0),
        # BO, These copy color of index 1
        (  0.0,  5.037, 1), #
        (  0.0,  5.0534, 1),
        (  0.0,  4.9690, 1),
        (  0.0,  4.9522, 1),
        #(  0.0,  4.789, 1), # Dropped because out of image
        (  0.0, 5.0699, 1),
        #
        # Older references:
        #
        #(300.0, 5.1013e-11, 0),
        #(300.0,  5.080e-11, 0),
        #(  0.0,   5.08e-11, 0),
        #(  0.0,   5.04e-11, 0),
        #(  0.0,   5.01e-11, 0),
        #(  0.0,  5.061e-11, 1),
        #(  0.0, 5.0530e-11, 1),
        #(  0.0,  5.201e-11, 1),
        #(  0.0,  4.815e-11, 1),
    )
    plotting.plot_references(ax, refdata_H2)
    ax.legend()
    ax.set_ylim((None, 5.32))
    ax.set_xlim((-400, None))

if __name__ == "__main__":
    main()
    plt.show()
