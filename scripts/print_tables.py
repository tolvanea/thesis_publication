# Print all data in numerical tabular form. Also calculate tau=0 and T=0
# extrapolation from finite-tau data stored in JSONs.
import numpy as np

import matplotlib.pyplot as plt
from uncertainties import unumpy as unp, ufloat

from tools import extrapolation
from tools import plotting
from tools import printing
from tools import reading


def main():

    system_names = [
        "H_varT_AQ", "H_varT_BO", "H2_varT_AQ", "H2_varT_BO",
        "HD_varT", "D2_varT", "He_varT_AQ", "He_varT_BO", "Ps_varT", "Ps2_varT", "Ps2_varT_old_calculation",
        "Ps2_varM_T300", "Ps2_varM_T300_new_datapoints", "Ps2_varM_T1000", "Ps2_varM_T3000"
    ]

    for system_name in system_names:
        print("########################\n" + system_name + "\n")
        for estimator, unit in [("energy", "Hartree"), ("susceptibility", "m^3/mol")]:
            folder = "datas/" + system_name
            jsonfile = folder + "/" + estimator + "_data.json"
            tablefile = folder + "/" + estimator + "_tables.txt"

            # To generate jsons from cached data of my thesis' repo:
            # 0. create a directory to 'datas/', and make it the only item in 'system_names'
            # 1. Copy there 'cache' and 'configure_instance.py' from thesis' system
            # 2. Remove everything else than the class 'InitialSetup' from the latter
            # 3. Uncoment the three lines below:
            #from tools import unused_functions
            #system = unused_functions.read_cached_value(folder, estimator)
            #unused_functions.save_data(system, write_file=jsonfile)

            system = reading.load_system(jsonfile)
            assert (system.latex_symbol1 == "\\tau")
            system.add_tau0_extrapolation()
            if system.latex_symbol2 == "T":
                T0_extrapolation(system, system_name)

            printing.print_and_write_tables(
                system,
                write_file=None, # to re-write .txt tables, repalce 'None' with 'tablefile',
                title=estimator + " (" + unit + ")",
            )

def T0_extrapolation(system, system_name):
    # Extrapolate T=0K
    if system_name == "H2_varT_AQ":  # Exponential (not linear) extrapolation
        f = lambda x, y, e: extrapolation.power_coeff_errorbardata_fit(
            x, y, e, 0.0, c_lower_bound=y[0]*0.9, c_upper_bound=y[0]*1.1
        )
        system.add_tau0_T0_extrapolation(ext_fun=f)
    # BO systems are T-independent, take mean
    #elif system_name[-3:] == "_BO":
        #f = lambda x, y, e: extrapolation.proper_mean_with_errorbars(y, e)
        #system.add_tau0_T0_extrapolation(ext_fun=f)
    else:  # Linear T=0 extrapolation
        system.add_tau0_T0_extrapolation()


if __name__ == "__main__":
    main()
