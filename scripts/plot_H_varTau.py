# Plot H while variating finite timestep tau. Used to illustrate tau=0 extrapolation
import numpy as np
import matplotlib.pyplot as plt
import uncertainties as unp

from tools import extrapolation
from tools import plotting
from tools.plotting import dot, dash, XXX
from tools import printing
from tools import reading

def get_H_AQ_data_extrapolate_tau0():
    system = reading.load_system("datas/H_varT_AQ/susceptibility_data.json")
    system.add_tau0_extrapolation()
    return system

def main():
    sys = get_H_AQ_data_extrapolate_tau0()

    ax, _ = plotting.create_figure_axis("H_AQ_varTau")
    ax.set_xlabel("$\\Delta\\tau\\;(E_\\mathrm{h}^{-1})$")

    x = sys.range1
    y = sys.data[:, 0]
    err = sys.err[:, 0]
    y_tau0 = sys.data_tau0[0]
    err_tau0 = sys.err_tau0[0]
    color = "C0"

    # Print bare errorbars of data
    line, caps, bars = ax.errorbar(
        x, -1e11*y, 1e11*err, label="PIMC",
        ls="", c=color, capsize=4, **dot
    ) # $300\\;$K
    #plotting.set_errobar_fake_transparency(line, caps, bars, alpha=0.1)

    x_range = np.linspace(0.0, x[-1]*1.1, 41)
    # Fit curve to errorbars
    fit, fit_err = extrapolation.linear_errorbardata_fit(x, y, err, x_range)

    line0, caps0, bars0 = ax.errorbar(x_range, -1e11*fit, x_range*0, ls="--", c=color) # label="linear fit", lw=1
    plotting.hide_error_bar_with_fake_transparency(caps0, bars0)
    if False:  # confidense interval lines
        line0, caps0, bars0 = ax.errorbar(
            x_range, -1e11*fit - 1e11*fit_err, x_range*0,
            ls=":", c=color, label="$1\\sigma$ error of fit"
        )
        plotting.hide_error_bar_with_fake_transparency(caps0, bars0)
        ax.plot(x_range, -1e11*fit+ 1e11*fit_err, ls=":", c=color,)

    line0, caps0, bars0 = ax.errorbar(
        0.0, -1e11*y_tau0, 1e11*err_tau0, #label="$\\Delta\\tau=0$ estimate",
        ls="", c="C0", capsize=4, **dot
    )
    plotting.set_errobar_fake_transparency(line0, caps0, bars0, alpha=0.4)

    refdata = [
        # x,    y,           label,                   color, marker
        #( 0.0,  2.98583, ("reference", "grey", "o",)), # analytical BO # $0\\;$K
        ( 0.0,  2.99071, ("Reference", "C1", *XXX.values())), # AQ

    ]
    ax.set_ylim((None,3.01))  # More room for legend box
    plotting.plot_references(ax, refdata)
    ax.legend()
    plt.show()


if __name__ == "__main__":
    main()

