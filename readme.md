# Thesis publication, diamagnetic susceptibility

**The final version of paper sent to publisher is `final_draft.pdf`. Read that.**

Published version:
https://journals.aps.org/pra/abstract/10.1103/PhysRevA.105.022816

This repository contains LaTex source-files and python-scripts to draw all images and print tables. Also, scripts contain all conversion factors to reference values from other sources.

This work is based on my master's thesis:
https://gitlab.com/tolvanea/msc-thesis

The document is also mirrored to overleaf
https://www.overleaf.com/read/prfhwdjnxzjh
